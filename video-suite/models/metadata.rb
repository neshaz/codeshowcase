class Metadata < ActiveRecord::Base
  # Attributes
  attr_accessible :digital_asset_id, :title, :description, :year_of_production, :categories, :cast, :director, :customer_key, :guid, :published_date,
                  :collection_ids, :selectedForm, :id, :category_id, :language_id, :genre_id, :duration, :publish_from, :publish_to, :adult, :tags,
                  :custom_field_values_attributes, :metadata_translations_attributes, :published

  attr_accessor :title, :selectedForm

  attr_accessor :duration_h, :duration_m, :duration_s

  # Validations
  # validates :digital_asset_id, presence: true

  # Associations
  belongs_to :digital_asset
  belongs_to :raw_digital_asset, :foreign_key => 'digital_asset_id'
  belongs_to :category
  belongs_to :language
  belongs_to :genre

  has_many :custom_field_values
  accepts_nested_attributes_for :custom_field_values, allow_destroy: true

  has_many :metadata_translations
  accepts_nested_attributes_for :metadata_translations, allow_destroy: true

  scope :published, -> { where(published: true) }

  # Callbacks
  after_initialize :get_title
  after_save :update_content_title
  after_save :set_custom_fields

  def duration_h
    duration.nil? ? "" : duration/3600
  end

  def duration_m
    duration.nil? ? "" : (duration/60)-duration_h*60
  end

  def duration_s
    duration.nil? ? "" : duration-duration_m*60-duration_h*3600
  end

  private
  def get_title
    self.title = self.raw_digital_asset.title if self.raw_digital_asset
  end

  def update_content_title
    self.raw_digital_asset.update_attribute(:title, self.title) if self.raw_digital_asset
    self.raw_digital_asset.save if self.raw_digital_asset
  end

  def set_custom_fields
    digital_asset.content_owner.custom_fields.all.each do |custom_field|
      cfv = custom_field_values.where(custom_field_id: custom_field.id).first
      custom_field_values.create(custom_field_id: custom_field.id, value: "") if cfv.nil?
    end
  end
end
