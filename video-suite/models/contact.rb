class Contact < ActiveRecord::Base
  self.inheritance_column = 'contact_type'

  attr_accessible :address, :avatar, :contact_type, :email, :name, :user_id, :related_user_id, :create_type, :old_contact_id
  attr_accessor :create_type

  # Uploaders
  mount_uploader :avatar, AvatarUploader

  # Validations
  validates :user_id, presence: true
  validates :name, presence: true, unless: "create_type == 'share'", length: { maximum: 50 }#, uniqueness: { scope: :user_id }
  validates :email, presence: true, if: "contact_type == 'IndividualContact'", uniqueness: { scope: :user_id }, email_format: true

  # Associations
  belongs_to :user
  belongs_to :related_user, :foreign_key => 'related_user_id', :class_name => "User"
  has_many :shares, :dependent => :destroy
  has_many :associated_contacts, :foreign_key => 'contact_id', class_name: "RelatedContact", dependent: :destroy
  has_many :related_contacts, :foreign_key => 'relation_id', class_name: "RelatedContact", dependent: :destroy
  has_many :contact_members, through: :associated_contacts
  has_many :contact_sets, through: :related_contacts

  # Scopes
  scope :individual_contacts, where('contact_type =?', "IndividualContact")
  scope :contact_organizations, where('contact_type =?', "ContactOrganization")
  scope :contact_groups, where('contact_type =?', "ContactGroup")

  # Callbacks
  before_save :lowercase_email

  def first_name
    name.nil? ? self.related_user.first_name : name.split(' ').first
  end

  def last_video_notification
    Notification.where(:user_id => self.related_user_id,
                       :notification_type => "watched",
                       :content_type => "Video").last
  end

  def last_video_notifications
    Notification.where(:user_id => self.related_user_id,
                      :notification_type => "watched",
                      :content_type => "Video").order('created_at DESC')

  end

  def last_video
    Video.where(:id => last_video_notification.content_id).last unless last_video_notification.nil?
  end

  def recent_videos
    recent_videos_ids = last_video_notifications.map(&:content_id).uniq.slice(1, 6)
    Video.find_all_by_id(recent_videos_ids)
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end

  def lowercase_email
    self.email = email.downcase if self.email.present?
  end

  def self.shares(item_shares)
    where('id IN (?)', item_shares.map(&:contact_id).compact)
  end

  def has_review_count_access_for?(video)
    results = VideoReviewAccess.where(approvable_id: self, digital_asset_id: video)
    return true if results.size > 0
    return false
  end

  def self.collect_individual_contacts(contacts)
    Contact.find_by_sql ["select distinct c.id, c.name, c.contact_type from contacts c, shares s where c.id = s.contact_id and c.contact_type='IndividualContact' and s.contact_id in (?) union select distinct c.id, c.name, c.contact_type from contacts c where c.id in (select distinct r.relation_id from related_contacts r, shares s where r.contact_id = s.contact_id and s.contact_id in (?))", contacts, contacts]
  end

end
