class NotificationsController < ApplicationController
  def index
  end

  def notification
    notification_id = Remodel.redis.lindex "user:#{current_user.id}:new_notifications_ids", params[:index]
    user_notification = current_user.fetch_notification(notification_id)
    if user_notification.nil? && params[:type].nil?
      render :text => ''
    else
      render :text => render_notification(user_notification, params[:type], true)
    end
  end

  def unread
    render :json => {unread: current_user.new_notifications_ids.values.length}
  end

  def readall
    Remodel.redis.del "user:#{current_user.id}:new_notifications_ids"
    render :json => {unread: current_user.new_notifications_ids.values.length}
  end

  def get_notifications
    @content_owners = current_user.available_content_owners(@current_owner_module.id)
    session[:submodules] = @content_owners.map(&:submodule)

    filters = get_filter_periods(params[:filter])
    new_notifications_ids = current_user.new_notifications_ids.values

    start_impression = params["start"].nil? ? 0 : params["start"]
    notifications = current_user.notifications(start_impression, params[:limit], filters[:start_period], filters[:end_period])
    notifications_html = ""
    notifications.each do |n|
      # if params[:owner_module] == n.owner_module
      new_notification = new_notifications_ids.include?(n.id.to_s)
      notifications_html << render_notification(n, params[:type], new_notification)
      # end
    end

    render :text => notifications_html
  end

  private
  def render_notification (notification, type, new_notification)
    if type == 'dashboard'
      render_to_string :partial => 'shared/notifications/notification_dashboard', :locals => {:notification => notification, :new_notification => new_notification}
    elsif type == 'header'
      render_to_string :partial => 'shared/notifications/notification_header', :locals => {:notification => notification, :new_notification => new_notification}
    elsif type == 'notifications_index'
      render_to_string :partial => 'shared/notifications/notification_index', :locals => {:notification => notification, :new_notification => new_notification}
    end
  end

  def get_filter_periods (filter_type=nil)
    filter_type = filter_type.nil? ? 'all' : filter_type
    case filter_type
      when 'all'
        {start_period: nil, end_period: nil}
      when 'year'
        {start_period: DateTime.now.beginning_of_year, end_period: DateTime.now.end_of_year}
      when 'month'
        {start_period: DateTime.now.beginning_of_month, end_period: DateTime.now.end_of_month}
      when 'week'
        {start_period: DateTime.now.beginning_of_week, end_period: DateTime.now.end_of_week}
      when '24hours'
        {start_period: DateTime.now - 24.hours, end_period: DateTime.now.end_of_day}
    end
  end
end
