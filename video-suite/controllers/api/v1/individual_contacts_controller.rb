class Api::V1::IndividualContactsController < Api::V1::BaseController

  # Has scope
  has_scope :by_last_update

  # Inherited resources
  inherit_resources

  def index
    @individual_contacts = current_resource_user.individual_contacts.
      includes(:related_user, :contact_sets)
    respond_to do |format|
      if !@individual_contacts.nil?
        format.json { respond_with @individual_contacts }
      else
        format.json { render :json => "Error", :status => :unprocessable_entity }
      end
    end
  end

  def show
    @individual_contact = current_resource_user.individual_contacts.find_by_id(params[:id])

    respond_to do |format|
      if !@individual_contact.nil?
        format.json { respond_with @individual_contact }
      else
        format.json { render :json => "Error: Individual Contact not found.", :status => :unprocessable_entity }
      end
    end
  end

  def create
    user = User.find_by_email(params[:individual_contact][:email])
    if user.present?
      @individual_contact = Contact.new(user_id: current_resource_user.id, name: "#{user.first_name} #{user.last_name}", avatar: user.avatar, email: user.email, related_user_id: user.id, contact_type: 'IndividualContact')
    else
      @individual_contact = current_resource_user.contacts.new(name: "#{params[:individual_contact][:first_name]} #{params[:individual_contact][:last_name]}", contact_type: 'IndividualContact', email: params[:individual_contact][:email], avatar: params[:individual_contact][:avatar])
    end

    respond_to do |format|
      if @individual_contact.save
        format.json { render :json => @individual_contact, :status => :ok }
      else
        format.json { render :json => "Error: Missing parameters.", :status => :unprocessable_entity }
      end
    end
  end

end
