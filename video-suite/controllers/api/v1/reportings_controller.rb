class Api::V1::ReportingsController < Api::V1::BaseController
  before_filter :set_basic_color

  def chart
    if params[:period].present?
      @period = params[:period]
    else
      @period = 'day'
    end

    if params[:view].present?
      @view = params[:view]
    else
      @view = 'by'
    end

    setup_chart
    @chart_type = params[:chart_type]

    case params[:chart_type]
      when 'watched_videos'
        watched_videos
      when 'co_watched_videos'
        co_watched_videos
      when 'starred_videos'
        starred_videos
      when 'shared_videos'
        shared_videos
      when 'video_views'
        video_views
        render 'video_views'
      when 'video_engagement'
        video_engagement
        render 'video_engagement'
      when 'orders_placed'
        orders_placed
        render 'orders_placed'
      when 'sharing_velocity'
        sharing_velocity
        render 'sharing_velocity'
      when 'views_by_device'
        views_by_device
        render 'views_by_device'
      when 'geographical_usage'
        geographical_usage
        render 'geographical_usage'
      when "campaign_data"
        campaign_data
        render "campaign_data"
    end

  end

  def setup_chart
    case @period
    when 'day'
      @categories = ['00 h.', '01 h.', '02 h.','03 h.', '04 h.', '05 h.',
                   '06 h.', '07 h.', '08 h.','09 h.', '10 h.', '11 h.',
                   '12 h.', '13 h.', '14 h.','15 h.', '16 h.', '17 h.',
                   '18 h.', '19 h.', '20 h.','21 h.', '22 h.', '23 h.']

      @step = 3
    when 'week'
      @categories = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      @step = 1
    when 'month'
      cat = []

      d1 = Date.new(Date.today.year, Date.today.month, 1)
      d2 = Date.today.end_of_month

      (d1..d2).each do |d|
        cat << d.strftime("%-d %b")
      end

      @categories = cat

      @step = 7
    when 'year'
      @categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                     'Jul', 'Aug', 'Sep', 'Oct', 'Noe', 'Dec']

      @step = 1
    end
    @colors = [@color]
  end

  def watched_videos
    case @period
    when 'day'
      @data = get_data "watched_videos", "day"
    when 'week'
      @data = get_data "watched_videos", "week"
    when 'month'
      @data = get_data "watched_videos", "month"
    when 'year'
      @data = get_data "watched_videos", "year"
    end

    @graph = "chart1"
    @total = @data.inject(:+)
  end

  def co_watched_videos
    case @period
    when 'day'
      @data = get_data "co_watched_videos", "day"
    when 'week'
      @data = get_data "co_watched_videos", "week"
    when 'month'
      @data = get_data "co_watched_videos", "month"
    when 'year'
      @data = get_data "co_watched_videos", "year"
    end

    @graph = "chart2"
    @total = @data.inject(:+)
  end

  def starred_videos
    case @period
    when 'day'
      case @view
      when 'by'
        @data = get_data 'starred_videos', 'day', 'by'
      when 'to'
        @data = get_data 'starred_videos', 'day', 'to'
      end
    when 'week'
      case @view
      when 'by'
        @data = get_data 'starred_videos', 'week', 'by'
      when 'to'
        @data = get_data 'starred_videos', 'week', 'to'
      end
    when 'month'
      case @view
      when 'by'
        @data = get_data 'starred_videos', 'month', 'by'
      when 'to'
        @data = get_data 'starred_videos', 'month', 'to'
      end
    when 'year'
      case @view
      when 'by'
        @data = get_data 'starred_videos', 'year', 'by'
      when 'to'
        @data = get_data 'starred_videos', 'year', 'to'
      end
    end

    @graph = "chart3"
    @total = @data.inject(:+)
  end

  def shared_videos
    case @period
    when 'day'
      case @view
      when 'by'
        @data = get_shared_data 'day', 'by'
      when 'to'
        @data = get_shared_data 'day', 'to'
      end
    when 'week'
      case @view
      when 'by'
        @data = get_shared_data 'week', 'by'
      when 'to'
        @data = get_shared_data 'week', 'to'
      end
    when 'month'
      case @view
      when 'by'
        @data = get_shared_data 'month', 'by'
      when 'to'
        @data = get_shared_data 'month', 'to'
      end
    when 'year'
      case @view
      when 'by'
        @data = get_shared_data 'year', 'by'
      when 'to'
        @data = get_shared_data 'year', 'to'
      end
    end

    @graph = "chart4"
    @total = @data.inject(:+)
  end

  def video_views
    case @period
    when 'day'
      @data = get_video_views_data "day"
    when 'week'
      @data = get_video_views_data "week"
    when 'month'
      @data = get_video_views_data "month"
    when 'year'
      @data = get_video_views_data "year"
    end

    @total = @data.inject(:+)
  end

  def video_engagement
    @data = Array.new(11,0)
    @total = @data.inject(:+)
  end

  def orders_placed
    case @period
    when 'day'
      @data = Array.new(24,0)
    when 'week'
      @data = Array.new(7,0)
    when 'month'
      @data = Array.new(31,0)
    when 'year'
      @data = Array.new(12,0)
    end

    @total = @data.inject(:+)
  end

  def sharing_velocity
    @data = Array.new(7,0)
    @categories = ['1', '2', '3', '4', '5']
  end

  def views_by_device
    @data = [['Desktop', 0], ['Tablet', 0],
              ['Mobile', 0], ['Smart TV', 0]]
    @colors = ['#ff8f00','#e64444',@color,'#bcdc5b']
  end

  def geographical_usage
    @data = [
    { id: "NA", value: "0" },
    { id: "SA", value: "0" },
    { id: "AS", value: "0" },
    { id: "EU", value: "0" },
    { id: "AF", value: "0" },
    { id: "AU", value: "0"}]
  end

  def campaign_data
    period = params[:period] ? params[:period] : 0
    sort = params[:sort] ? params[:sort] : ""
    order = params[:order] ? params[:order] : "ascending"
    @content_type = params[:content_type] == "0" ? "Video" : "Playlist"
    if @content_type == "Video"
      @content = @resource_type.classify.constantize.outbox_content(current_resource_user, @current_owner_module.id, "videos")
    else
      @content = Playlist.outbox_playlists(current_resource_user, @current_owner_module.id)
      video_playlists = VideoPlaylist.where("playlist_id IN (?)", @content.map(&:id))
      @videos = Video.where("id IN (?)", video_playlists.map(&:video_id))
    end

    # if @current_owner_module.url_path == "review_and_approve" || @current_owner_module.url_path == "secure_online_dailies"
      @content.each do |item|
        load_video_review_data(item, period, @content_type)
      end
    # end

    @content = @content.reject{ |item| item unless item.campaigns_count != 0  }

    if sort.empty?
      if @content_type == "Video"
        @content = @content.sort_by &:title
      else
        @content = @content.sort_by &:name
      end
    else
      @content = @content.sort_by &sort.to_sym
    end
    if order == "descending"
      @content = @content.reverse
    end

    if @current_owner_module.url_path == "screening" || @current_owner_module.url_path == "b2b"
      @content.each do |item|
        load_b2b_screening_data(item)
      end
    end
  end

  def load_content_campaigns
    period = params[:period]
    @content_type = params[:content_type] == "0" ? "Video" : "Playlist"
    @shares = current_resource_user.content_shares(@current_owner_module.id, @content_type).where("shareable_id = ?", params[:id]).order("campaign_flag DESC")
    if period != "0"
      @shares = get_shares_by_period(@shares, period)
    end
    arr_results = []
    campaign_flags = @shares.map(&:campaign_flag).compact.uniq
    campaign_flags.each do |cf|
      campaign_shares = @shares.where(campaign_flag: cf)

      individual_contact_ids = campaign_shares.map(&:individual_contact_id).compact
      contact_organization_ids = campaign_shares.map(&:contact_organization_id).compact
      contact_group_ids = campaign_shares.map(&:contact_group_id).compact

      i_user_ids = IndividualContact.find(individual_contact_ids).map(&:contact_user_id)
      co_user_ids = IndividualContact.where('contact_organization_id IN (?)', contact_organization_ids).map(&:contact_user_id)
      cg_user_ids = IndividualContact.includes(:group_contacts).where('group_contacts.contact_group_id IN (?)', contact_group_ids).map(&:contact_user_id)

      user_ids = i_user_ids + co_user_ids + cg_user_ids

      if @content_type == "Video"
        results = Review.get_approve_reject_by_campaign_flag(cf, params[:id], @current_owner_module.id, user_ids)
        approves = results[:approves]
        rejects = results[:rejects]
        video = Video.find params[:id]
        title = video.title.truncate(45)
        if @current_owner_module.url_path == "screening" || @current_owner_module.url_path == "b2b"
          results = load_b2b_screening_campaign_data(video, @shares, cf)
          uploaded = results[:uploaded].count
          viewed = results[:viewed].count
          shared = results[:shared].count
          pending = results[:pending].count
          ordered = results[:orders_count]
        else
          results = VideoReview.get_approve_reject_by_campaign_flag(cf, params[:id], @current_owner_module.id, user_ids)
          approves = results[:approves]
          rejects = results[:rejects]
          wait = user_ids.count - (approves + rejects)
        end
      elsif @content_type == "Playlist"
        playlist = Playlist.find params[:id]
        title = playlist.name.truncate(45)
        videos = playlist.videos
        approves, rejects, viewed, shared, pending, ordered = 0, 0, 0, 0, 0, 0
        videos.each do |video|
          results = Review.get_approve_reject_by_campaign_flag(cf, video.id, @current_owner_module.id, [])
          approves = approves + results[:approves]
          rejects = rejects + results[:rejects]
          results = load_b2b_screening_campaign_data(video, @shares, cf)
          viewed = viewed + results[:viewed].count
          shared = shared + results[:shared].count
          ordered = ordered + results[:orders_count]
        end
        wait = playlist.videos.count - (approves + rejects)
        pending = shared - viewed
      end

      review_status = eval(AccessContent.where("targetable_id IN (?) AND targetable_type = ?", campaign_shares.map(&:id), "Share").first.properties["review_locked"])

      campaign_date = campaign_shares.first.created_at.strftime("%b %d, %Y")

      arr_results << { campaign: cf, approves: approves, rejects: rejects, wait: wait, campaign_date: campaign_date, review_status: review_status, truncated_title: title, uploaded: uploaded, viewed: viewed, pending: pending, shared: shared, ordered: ordered }
    end
    render json: arr_results
  end

  private

  def load_b2b_screening_data(item)
    viewed, uploaded, shared, ordered = [], [], [], []
    pending = 0
    item.shared = 0
    if @current_owner_module.url_path == "screening" || @current_owner_module.url_path == "b2b"
      if item.class.name == "Video"
        items = [item]
      else
        items = item.videos
      end
      items.each do |i|
        i.impression_ids.members.each do |id|
          imp = Impression.find($redis_context, id.to_i)
          if imp.owner_module == @current_owner_module.url_path
            case imp.message
            when "watched"
              if imp.user_id != current_resource_user.id
                if item.class.name == "Video"
                  viewed << imp
                end
                if imp.referrer && imp.referrer.include?("playlist_id") && item.class.name == "Video"
                else
                  viewed << imp
                end
              end
            when "uploaded"
              uploaded << imp
            when "placed an order for"
              if item.class.name == "Video"
                if imp.referrer && imp.referrer.include?("playlist_id")
                else
                  ordered << imp
                end
              else
                if imp.referrer && !imp.referrer.include?("playlist_id")
                else
                  ordered << imp
                end
              end
            end
          end
        end
      end
      arr_viewed = []
      viewed.each do |v|
        arr_viewed << [v.user_id, v.impressionable_id]
      end
      arr_viewed.uniq!
      viewed.uniq!

      shares = current_resource_user.content_shares(@current_owner_module.id, item.class.name).where("shareable_id = ?", item.id)
      shares_count = 0
      shares.each do |share|
        if share.contact_share?
          shares_count = shares_count + 1
        elsif share.organization_share?
          organization = ContactOrganization.find(share.contact_organization_id)
          shares_count = shares_count + organization.individual_contacts.count
        else
          group = ContactGroup.find(share.contact_group_id)
          shares_count = shares_count + group.individual_contacts.count
        end
      end
      if item.class.name == "Video"
        item.shared = shares_count + shared.count
        item.pending = item.shared - arr_viewed.count
        item.viewed = arr_viewed.count
      else
        item.shared = item.videos.count * shares_count
        item.pending = item.shared - arr_viewed.count
        item.viewed = item.shared - item.pending
      end
    end
    item.uploaded = uploaded.count
        item.ordered = ordered.count
  end

  def get_shares_by_period(shares, period)
      case period
      when "1"
        shares = shares.where("DATE(created_at) = ?", Date.today)
      when "2"
        from = Date.today.beginning_of_week
        to = from + 7.days
      when "3"
        from = Date.today.beginning_of_month
        to = Date.today.beginning_of_month.next_month
      when "4"
        from = Date.today.beginning_of_year
        to = Date.today.end_of_year
      end
      if from
        shares = shares.where("DATE(created_at) BETWEEN ? AND ?", from, to)
      end
      shares
  end

  def load_video_review_data(item, period, content_type)
    properties = item.access_content.where('targetable_type =? AND targetable_id =? AND owner_module_id =?', 'User', current_user.id, @current_owner_module.id)
    if properties.empty?
      item.campaigns_count = 0
    else
      all_shares = current_resource_user.content_shares(@current_owner_module.id, content_type).where("shareable_id = ?", item.id).order("created_at DESC")
      if period != 0
        all_shares = get_shares_by_period(all_shares, period)
      end
      if all_shares.empty?
        item.campaign_date = ""
        item.wait = 0
        item.review_locked = false
        item.campaigns_count = 0
      else
        if properties.empty?
          item.campaigns_count = 0
        else
          campaign_flags = all_shares.map(&:campaign_flag)
          item.campaign_date = all_shares.first.created_at.strftime("%b %d, %Y")
          if content_type == "Video"
            results = Review.get_approve_reject_by_campaign_flag(campaign_flags, item.id, @current_owner_module.id, [])
            item.approves = results[:approves]
            item.rejects = results[:rejects]
            item.wait =  all_shares.count - (item.approves + item.rejects)
          else
            videos = item.videos
            item.approves = 0
            item.rejects = 0
            videos.each do |video|
              results = Review.get_approve_reject_by_campaign_flag(campaign_flags, video.id, @current_owner_module.id, [])
              item.approves = item.approves + results[:approves]
              item.rejects = item.rejects + results[:rejects]
            end
            item.wait = (all_shares.count * item.videos.count) - (item.approves + item.rejects)
          end
          item.review_locked = eval(item.access_content.where('targetable_type =? AND targetable_id =? AND owner_module_id =?', 'User', current_user.id, @current_owner_module.id).first.properties["review_locked"])
          item.campaigns_count = all_shares.map(&:campaign_flag).uniq.count
        end
      end
    end
  end

  def set_basic_color
    if @submodule == 'rcr'
      @color = "#E29228"
    elsif @submodule == 'entonegroup' || @submodule == 'hbo' || @submodule == 'paramount' || @submodule == 'snd'
      @color = "#2E8DCA"
    elsif @submodule == 'img'
      @color = "#42688D"
    elsif @submodule == 'one-potato-two-potato'
      @color = "#ff28a2"
    elsif @submodule == 'marvista'
      @color = "#e43d23"
    elsif @submodule == 'tata-communications'
      @color = "#7bae5b"
    else
      @color = "#0BA4B6"
    end
  end

  def setup_period(period)
    case period
    when 'day'
      data = Array.new(24, 0)
    when 'week'
      data = Array.new(7, 0)
      week_array = Array.new

      d1 = Date.today - Date.today.wday + 1 #first day of current week
      d2 = Date.today + 7 - Date.today.wday #end of current week

      (d1..d2).each do |d|
        week_array << d
      end
    when 'month'
      data = Array.new(Date.today.end_of_month.day, 0)
    when 'year'
      data = Array.new(12, 0)
    end

    data
  end

  def get_data(type, period, view = nil)
    case period
    when 'day'
      data = Array.new(24, 0)
    when 'week'
      data = Array.new(7, 0)
      week_array = Array.new

      d1 = Date.today - Date.today.wday + 1 #first day of current week
      d2 = Date.today + 7 - Date.today.wday #end of current week

      (d1..d2).each do |d|
        week_array << d
      end
    when 'month'
      data = Array.new(Date.today.end_of_month.day, 0)
    when 'year'
      data = Array.new(12, 0)
    end

    case type
      when 'watched_videos'
          videos = current_resource_user.videos.where(:original_owner_module_id => @current_owner_module.id)
          search_for = "watched"
          user_cond = "imp.user_id != current_resource_user.id"

      when 'co_watched_videos'
        if current_resource_user.content_owner.present?
          videos = current_resource_user.content_owner.videos.where(:original_owner_module_id => @current_owner_module.id)
        else
          videos = []
        end
        search_for = "watched"
        user_cond = "imp.user_id != current_resource_user.id"
      when 'starred_videos'
        case view
        when 'by'
          videos = Video.all_with_access(current_resource_user, @current_owner_module.id, 'videos')
        when 'to'
          videos = Video.outbox_content(current_resource_user, @current_owner_module.id, 'videos')
        end
        search_for = "starred"
        user_cond = "imp.user_id == current_resource_user.id"
    end

    videos.each do |v|
      case type
      when 'starred_videos'
        impression_ids = Remodel.redis.zrange "video:#{v.id}:impression_ids", 0, -1
      else
        impression_ids = Remodel.redis.zrange "video:#{v.id}:impression_ids", 0, -1
      end
      impression_ids.each do |id|
        imp = Impression.find($redis_context, id.to_i)
        if imp.message == search_for && eval(user_cond) && imp.owner_module == @current_owner_module.url_path
          case period
          when "day"
            if imp.created_at.to_date == Date.today
              (0..23).each do |h|
                if h == Impression.find($redis_context, id.to_i).created_at.hour
                  data[h] += 1
                end
              end
            end

          when "week"
              if week_array.include?(imp.created_at.to_date)
                (0..6).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.wday
                    data[h] += 1
                  end
                end
              end

          when "month"
              if imp.created_at.year == Date.today.year && imp.created_at.month == Date.today.month
                (1..Date.today.end_of_month.day).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.day
                    data[h-1] += 1
                  end
                end
              end

          when "year"
              if imp.created_at.year == Date.today.year
                (1..12).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.month
                    data[h-1] += 1
                  end
                end
              end
          end
        end
      end
    end

    if type == "week"
      data.push(data.shift)
    end

    data
  end

  def get_shared_data(period, view = nil)
    case period
    when 'day'
      data = Array.new(24, 0)
    when 'week'
      data = Array.new(7, 0)
      week_array = Array.new

      d1 = Date.today - Date.today.wday + 1 #first day of current week
      d2 = Date.today + 7 - Date.today.wday #end of current week

      (d1..d2).each do |d|
        week_array << d
      end
    when 'month'
      data = Array.new(Date.today.end_of_month.day, 0)
    when 'year'
      data = Array.new(12, 0)
    end

    case view
    when 'by'
      impression_ids = Remodel.redis.lrange "user:#{current_resource_user.id}:notifications_ids", 0, -1
      impression_ids.each do |id|
        imp = Impression.find($redis_context, id.to_i)
        if imp.message == "shared" && imp.owner_module == @current_owner_module.url_path
          case period
          when "day"
            if imp.created_at.to_date == Date.today
              (0..23).each do |h|
                if h == Impression.find($redis_context, id.to_i).created_at.hour
                  data[h] += 1
                end
              end
            end

          when "week"
              if week_array.include?(imp.created_at.to_date)
                (0..6).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.wday
                    data[h] += 1
                  end
                end
              end

          when "month"
              if imp.created_at.year == Date.today.year && imp.created_at.month == Date.today.month
                (1..Date.today.end_of_month.day).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.day
                    data[h-1] += 1
                  end
                end
              end

          when "year"
              if imp.created_at.year == Date.today.year
                (1..12).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.month
                    data[h-1] += 1
                  end
                end
              end
          end
        end
      end
    when 'to'
      videos = DigitalAsset.outbox_content(current_resource_user, @current_owner_module.id, 'videos')
      videos.each do |v|
        case period
        when "day"
          if v.created_at.to_date == Date.today
            (0..23).each do |h|
              if h == v.created_at.hour
                data[h] += 1
              end
            end
          end

        when "week"
            if week_array.include?(v.created_at.to_date)
              (0..6).each do |h|
                if h == v.created_at.wday
                  data[h] += 1
                end
              end
            end

        when "month"
            if v.created_at.year == Date.today.year && v.created_at.month == Date.today.month
              (1..Date.today.end_of_month.day).each do |h|
                if h == v.created_at.day
                  data[h-1] += 1
                end
              end
            end

        when "year"
            if v.created_at.year == Date.today.year
              (1..12).each do |h|
                if h == v.created_at.month
                  data[h-1] += 1
                end
              end
            end
        end
      end
    end

    if period == "week"
      data.push(data.shift)
    end

    data
  end

  def get_video_views_data(type)
    if current_resource_user.content_owner.present?
      content_owner_videos = DigitalAsset.content_owner_content(current_resource_user, @current_owner_module.id, 'videos')
    else
      content_owner_videos = []
    end

    case type
    when "day"
      data = Array.new(24, 0)
    when "week"
      data = Array.new(7, 0)
      week_array = Array.new

      d1 = Date.today - Date.today.wday + 1 #first day of current week
      d2 = Date.today + 7 - Date.today.wday #end of current week

      (d1..d2).each do |d|
        week_array << d
      end
    when "month"
      data = Array.new(Date.today.end_of_month.day, 0)
    when "year"
      data = Array.new(12, 0)
    end

    content_owner_videos.each do |v|
      impression_ids = Remodel.redis.zrange "video:#{v.id}:impression_ids", 0, -1
      impression_ids.each do |id|
        imp = Impression.find($redis_context, id.to_i)
        if imp.message == "watched"
          case type
          when "day"
            if imp.created_at.to_date == Date.today
              (0..23).each do |h|
                if h == Impression.find($redis_context, id.to_i).created_at.hour
                  data[h] += 1
                end
              end
            end

          when "week"
              if week_array.include?(imp.created_at.to_date)
                (0..6).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.wday
                    data[h] += 1
                  end
                end
              end

          when "month"
              if imp.created_at.year == Date.today.year && imp.created_at.month == Date.today.month
                (1..Date.today.end_of_month.day).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.day
                    data[h-1] += 1
                  end
                end
              end

          when "year"
              if imp.created_at.year == Date.today.year
                (1..12).each do |h|
                  if h == Impression.find($redis_context, id.to_i).created_at.month
                    data[h-1] += 1
                  end
                end
              end
          end
        end
      end
    end

    if type == "week"
      data.push(data.shift)
    end

    data
  end
end
