class Api::V1::DashboardController < Api::V1::BaseController

  def viewed
    digital_assets = Video.with_access(current_resource_user, @current_owner_module.id, "videos")
    digital_asset_ids = digital_assets.count > 0 ? digital_assets.map(&:id) : []
    viewed_count = Notification.where("content_type = ? AND content_id IN (?) AND notification_type = 'watched' AND owner_module_id = ?", @resource_type.classify, digital_asset_ids, @current_owner_module.id).count
    render json: { viewed: viewed_count }
  end

  def shared
    digital_assets = Video.outbox_content(current_resource_user, @current_owner_module.id, @resource_type)
    digital_asset_ids = digital_assets.count > 0 ? digital_assets.map(&:id) : []
    shared_views_count = Notification.where("content_type = ? AND content_id IN (?) AND notification_type = 'watched' AND user_id != ? AND owner_module_id = ?", @resource_type.classify, digital_asset_ids, current_resource_user.id, @current_owner_module.id).count
    render json: { viewed: shared_views_count }
  end

  def chart
    if params[:period].present?
      @period = params[:period]
    else
      @period = 'day'
    end

    case params[:chart_type]
      when 'viewed_plays'
        viewed_plays
      when 'shared_video_views'
        shared_video_views
    end
  end

  def all
    @shared_video_views = get_data "day", "shared"
    @shared_total = @shared_video_views.inject(:+)

    @shared_video_views_week = get_data "week", "shared"
    @shared_total_week = @shared_video_views_week.inject(:+)

    @shared_video_views_month = get_data "month", "shared"
    @shared_total_month = @shared_video_views_month.inject(:+)

    @shared_video_views_year = get_data "year", "shared"
    @shared_total_year = @shared_video_views_year.inject(:+)

    @viewed_plays = get_data "day", "viewed"
    @viewed_total = @viewed_plays.inject(:+)

    @viewed_plays_week = get_data "week", "viewed"
    @viewed_total_week = @viewed_plays_week.inject(:+)

    @viewed_plays_month = get_data "month", "viewed"
    @viewed_total_month = @viewed_plays_month.inject(:+)

    @viewed_plays_year = get_data "year", "viewed"
    @viewed_total_year = @viewed_plays_year.inject(:+)


    @viewed_videos = get_viewed_videos
    @recent_videos = Video.with_access(current_resource_user, @current_owner_module.id, 'videos').order("created_at desc").limit(10)
  end

  def shared_video_views
    case @period
    when 'day'
      @data = get_data "day", "shared"
    when 'week'
      @data = get_data "week", "shared"
    when 'month'
      @data = get_data "month", "shared"
    when 'year'
      @data = get_data "year", "shared"
    end

    @total = @data.inject(:+)
  end

  def viewed_plays
    case @period
    when 'day'
      @data = get_data "day", "viewed"
    when 'week'
      @data = get_data "week", "viewed"
    when 'month'
      @data = get_data "month", "viewed"
    when 'year'
      @data = get_data "year", "viewed"
    end

    @total = @data.inject(:+)
  end

  def viewed_videos
    @videos = get_viewed_videos
    set_related_info

    render 'videos'
  end

  def recent_videos
    @videos = Video.with_access(current_resource_user, @current_owner_module.id, 'videos').order("created_at desc").limit(12)
    set_related_info

    render 'videos'
  end

  private

  def set_related_info
    @user = current_resource_user
    @video_stars = current_resource_user.content_stars_ids(@current_owner_module.id, 'Video')
  end

  def get_viewed_videos
    videos = DigitalAsset.joins("LEFT OUTER JOIN notifications ON notifications.content_id = digital_assets.id").where("notifications.content_type = 'Video' AND notifications.user_id = ? AND notifications.notification_type='watched' AND notifications.owner_module_id = ?", current_resource_user.id, @current_owner_module.id).order("notifications.created_at desc")
    results = videos.uniq_by { |v| v.id }[0..9]
  end

  def get_data(type, view)
    digital_assets = Video.user_content(current_resource_user, @current_owner_module.id, "videos")
    digital_asset_ids = digital_assets.count > 0 ? digital_assets.map(&:id) : []

    case type
    when "day"
      data = Array.new(24, 0)
    when "week"
      data = Array.new(7, 0)
      week_array = Array.new

      d1 = Date.today - Date.today.wday + 1 #first day of current week
      d2 = Date.today + 7 - Date.today.wday #end of current week

      (d1..d2).each do |d|
        week_array << d
      end
    when "month"
      data = Array.new(Date.today.end_of_month.day, 0)
    when "year"
      data = Array.new(12, 0)
    end

    case view
    when "viewed"
      notifications = Notification.where("content_type = 'Video' AND content_id IN (?) AND notification_type = 'watched' AND owner_module_id = ?", digital_asset_ids, @current_owner_module.id)
    when "shared"
      notifications = Notification.where("content_type = 'Video' AND content_id IN (?) AND notification_type = 'watched' AND user_id != ? AND owner_module_id = ?", digital_asset_ids, current_resource_user.id, @current_owner_module.id)
    end

    notifications.each do |n|
      case type
      when "day"
        if n.created_at.to_date == Date.today
          (0..23).each do |h|
            if h == n.created_at.hour
              data[h] += 1
            end
          end
        end
      when "week"
        if week_array.include?(n.created_at.to_date)
          (0..6).each do |h|
            if h == n.created_at.wday
              data[h] += 1
            end
          end
        end
      when "month"
        if n.created_at.year == Date.today.year && n.created_at.month == Date.today.month
          (1..Date.today.end_of_month.day).each do |h|
            if h == n.created_at.day
              data[h-1] += 1
            end
          end
        end
      when "year"
        if n.created_at.year == Date.today.year
          (1..12).each do |h|
            if h == n.created_at.month
              data[h-1] += 1
            end
          end
        end
      end
    end

    if type == "week"
      data.push(data.shift)
    end

    data
  end
end
