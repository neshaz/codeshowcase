class window.ContentUploader

  settings:
    uploaderForm: '#upload_content'
    uploads: '#uploads'
    uploadList: '.content-upload-list'
    submitButton: '.submit-uploads'
    cancelButton: '.cancel'
    uploadSuccessMesage: '.upload-success'
    contentUploadBox: '.content-upload-box'
    desktopDownload: '#content-desktop-download'
    actions: '.content-upload-actions'
    uploadThumb: '.content-upload-thumb'

  constructor: ->
    @attachUploader()
    @bindUIActions()
    @checkDownloadLink()

  bindUIActions: ->
   self = @
   $(@settings.uploads).find(@settings.submitButton).on "click", (event) ->
     self.submitForms()
   @bindUploadAdd()
   @bindUploadStart()
   @bindProgressBar()
   @bindSingleCompleted()
   @bindAllCompleted()

  # Disables the download link if not available
  checkDownloadLink: ->
    if $('.download-link').attr('href') == '#'
      $('.download-link').removeClass('primary')
      $('.download-link').addClass('secondary')
      $('.download-link').prop('disabled', true)

  attachUploader: ->
    self = @
    $(@settings.uploaderForm).S3Uploader
      remove_completed_progress_bar: false
      progress_bar_target: $(self.settings.uploadList)

  bindImageLoad: (el, context) ->
    $(el).on "change", (e) ->
      window.loadImage e.target.files[0], ((img) ->
        context.find('.content-upload-thumb img').remove()
        context.find('.content-upload-thumb').append(img)
        # context.find('.cotnent-upload-thumb-button').text("Change thumbnail")
      ),
      maxWidth: 600
      # $(el).fileupload "add",
      #   fileInput: $(@)

  bindUploadAdd: ->

    self = @
    $(@settings.uploaderForm).bind "fileuploadsend", (e, data) ->
      $(self.settings.submitButton).prop('disabled', true)
      thumbUpload = data.context.find('.thumbnail-upload')
      self.bindImageLoad(thumbUpload, data.context)
      self.bindFormSwitcher(data.context)
      self.bindDetailsToggle(data.context)
      $(thumbUpload).fileupload
        dataType: 'script'
        method: 'PUT'
        add: (e, data) ->
          console.log($(data.form.context).parent().parent().parent().find('.content-upload-thumb-button')[0])
          $($(data.form.context).parent().parent().parent().find('.content-upload-thumb-button')[0]).text("Uploading")
          data.submit()
        done: (e, data) ->
          $($(data.form.context).parent().parent().parent().find('.content-upload-thumb-button')[0]).text("Done")

      data.context.find('.select2').select2()
      data.context.find('.content-upload-title').html(data.files[0].name)
      data.context.find('.content_title').val(data.files[0].name)

  bindUploadStart: ->
    self = @
    $(@settings.uploaderForm).bind "fileuploadstart", (e, data) ->
      $(self.settings.actions).show()
      $(self.settings.desktopDownload).show()
      $(self.settings.uploads).show()

  bindProgressBar: ->
    $(@settings.uploaderForm).bind "fileuploadprogress", (e, data) ->
      progress = parseInt(data.loaded / data.total * 100, 10)
      $(data.context).find('.progress-percent').html("#{progress}% completed")

  bindSingleCompleted: ->
    self = @
    $(@settings.uploaderForm).bind "fileuploaddone", (e, data) ->
      content = self.buildContentObject(self.settings.uploaderForm, data.files[0], data.result)
      $(data.context).find('.content_file_url').val(content.url)
      $(data.context).find('.progress-percent').hide('slow')
      $(data.context).find('.progress').hide('slow')
      $(data.context).find('.progress-done').show('slow')

  bindFormSwitcher: (context) ->
    basicPicker = context.find('.form-basic')
    advancedPicker = context.find('.form-advanced')
    basicPicker.on "click", (e) ->
      basicPicker.toggleClass('active')
      advancedPicker.toggleClass('active')
      context.find('.content-upload-basic').show()
      context.find('.content-upload-advanced').hide()
      e.preventDefault()
    advancedPicker.on "click", (e) ->
      basicPicker.toggleClass('active')
      advancedPicker.toggleClass('active')
      context.find('.content-upload-basic').hide()
      context.find('.content-upload-advanced').show()
      e.preventDefault()

  bindDetailsToggle: (context) ->
    details = context.find('.content-upload-toggle')
    $(details).on "click", (e) ->
      context.find('.content-upload-form').toggle('fast')
      if $(details).text() == "Hide details"
        $(details).text("Show details")
      else
        $(details).text("Hide details")

  bindAllCompleted: ->
    self = @
    $(document).bind "s3_uploads_complete", ->
      $(self.settings.submitButton).prop('disabled', false)

  submitForms: ->
    self = @
    $(@settings.submitButton).unbind()
    $(@settings.submitButton).text("Confirming uploads...")
    $(@settings.submitButton).prop('disabled', true)
    $(@settings.cancelButton).hide()
    $('.content-upload-button').hide('fast')
    $('.dashboard-content-container').hide('fast')
    $('.tab-content').hide('fast')
    $('.progress-done').text("Please wait while your upload is being completed.")
    contentUploadBoxes = $(@settings.uploadList).find(@settings.contentUploadBox)
    contentUploadBoxes.each (index, uploadBox) ->
      contentTitle = $(uploadBox).find('.content_title').val()
      $(uploadBox).find('.content-upload-title').text(contentTitle)
      $(uploadBox).find('form').submit()
    setTimeout ( ->

      #$(self.settings.uploadThumb).show('slow')
    ), 5000


  buildContentObject: ($uploadForm, file, result) ->
    content = {}
    if result # Use the S3 response to set the URL to avoid character encodings bugs
      content.url      = $(result).find("Location").text()
      content.filepath = $('<a />').attr('href', content.url)[0].pathname
    else # IE <= 9 return a null result object so we use the file object instead
      domain           = $uploadForm.attr('action')
      content.filepath = settings.path + $uploadForm.find('input[name=key]').val().replace('/${filename}', '')
      content.url      = domain + content.filepath + '/' + encodeURIComponent(file.name)

    content.filename   = file.name
    content.filesize   = file.size if 'size' of file
    content.filetype   = file.type if 'type' of file
    content.unique_id  = file.unique_id if 'unique_id' of file
    # content = $.extend content, settings.additional_data if settings.additional_data
    content

# jQuery ->
#   $('#upload_videos').fileupload
#     dataType: 'script'
#     add: (e, data) ->
#       file = data.files[0]
#       data.context = $(tmpl("template-upload", file))
#       $('.js-video-upload-form').append(data.context)
#       containers = $('.js-video-upload-container')
#       forms = containers.last().find('form')
#       bindVideoFormEvents(forms.first())
#       $(forms).find('.js-form-field').val(containers.length)
#       $(forms).find('.js-playlist-select').attr('id', 'playlist_ids' + containers.length)
#       $('.js-browse-video').find('.js-browse-form').val(containers.length)
#       $(".chzn-select").chosen()
#       # $('.video-upload-preview').spin()
#       jqXHR = data.submit()
#       abortUpload(jqXHR)
#     progress: (e, data) ->
#       if data.context
#         progress = parseInt(data.loaded / data.total * 100, 10)
#         data.context.find('.text').text(progress + '% completed')
#         data.context.find('.bar').css('width', progress + '%')
#         #$('.progress').text(progress + ' %')
#     error: (jqXHR, textStatus, errorThrown) ->
#       console.log("Canceled upload")
#     done: (e, data) ->
#       data.context.find('.text').text('100% completed')
#       data.context.find('.bar').css('width', '100%')
#       # alert("Upload done!")
#
# abortUpload = (jqXHR) ->
#   $(".js-cancel-video-upload").live "click", (e) ->
#     e.preventDefault
#     jqXHR.abort()
#     $(this).parents('.js-video-upload-segment').slideUp()
#
# window.bindVideoFormEvents = (form) ->
#   form.fileupload
#     dataType: 'script'
#     done: (e, data) ->
#       console.log('thumbnail uploaded')
#

