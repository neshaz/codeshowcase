class window.OrdersBatchActions

  settings:
    batchActions: $('.js-orders-batch-actions-nav')
    allOrdersSelector: '#all_orders'
    ordersCheckboxes: '.js-order-checkbox'
    cancelOrder: '#js-cancel-order-button'
    fetchOrders: '#js-orders-fetch-button'
    fromFilter: '#js-orders-from-filter'
    toFilter: '#js-orders-to-filter'
    refreshOrders: '#js-orders-refresh-picker'

  init: ->
    @bindUIActions()

  toggleSelectAll: ->
    self = @
    $(@settings.ordersCheckboxes).each (index, checkbox) ->
      if $(self.settings.allOrdersSelector).prop('checked')
        $(checkbox).prop('checked', true)
      else
        $(checkbox).prop('checked', false)

  bindUIActions: ->
    self = @

    # bind events
    $(@settings.allOrdersSelector).on "click", (event) ->
      self.toggleSelectAll()

    $(@settings.cancelOrder).on "click", (event) ->
      event.preventDefault()
      self.cancelOrder()

    $(@settings.fetchOrders).on "click", (event) ->
      event.preventDefault()
      self.fetchOrders()

    $(@settings.refreshOrders).on "click", (event) ->
      event.preventDefault()
      self.refreshOrders()

    # disable all_orders selector
    count = $(@settings.ordersCheckboxes).length
    if count == 0
      $(@settings.allOrdersSelector).prop('disabled', true)

    # init datetime pickers
    $(@settings.fromFilter).datepicker({ dateFormat: 'dd/mm/yy' })
    $(@settings.toFilter).datepicker({ dateFormat: 'dd/mm/yy' })

  cancelOrder: ->
    self = @
    orderIds = []
    loaderDiv = $('.loading-gif')

    if confirm("Are you sure you want to cancel?")
      loaderDiv.show()

      $("#{@settings.ordersCheckboxes}:checked").each (index, checkbox) ->
        orderId = $(checkbox).val()
        orderIds.push orderId

      $.ajax
        type: 'delete'
        dataType: 'script'
        url: "/archive/orders/#{orderIds.join(',')}"
        success: ->
          orderIds.forEach (id) ->
            $("li#order_#{id}").remove()

          loaderDiv.hide()

    window.showHideActions(false)
    $(@settings.allOrdersSelector).prop('checked', false)

  fetchOrders: ->
    v1 = $(@settings.fromFilter).val()
    v2 = $(@settings.toFilter).val()

    loaderDiv = $('.loading-gif')

    if v1 != "From" && v2 != "To"
      loaderDiv.show()

      $.ajax
        type: 'post'
        dataType: 'script'
        data: { from: v1, to: v2 }
        url: "/archive/orders/fetch_orders"
        success: ->
          loaderDiv.hide()

  refreshOrders: ->
    loaderDiv = $('.loading-gif')
    loaderDiv.show()

    $.ajax
      url: "/archive/orders/refresh_orders"
      success: ->
        loaderDiv.hide()
