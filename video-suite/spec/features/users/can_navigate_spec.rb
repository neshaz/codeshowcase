require 'spec_helper'

feature "Authenticated user can edit video geotargeting" do

  background do
    authenticated_user
    FactoryGirl.create(:content_owner_member, :user => @user, :content_owner => FactoryGirl.create(:content_owner))
    ContentOwner.all.each do |co|
      OwnerModule.all.each do |om|
        om.owner_module_members.create(:content_owner_id => co.id)
      end
    end
    @video = FactoryGirl.create(:video, :user => @user, :content_owner => @user.content_owner, :title => "Taxi")
  end


  context "Top navigational links" do
    before do
      visit('/')
    end

    scenario "See all pages on top menu", :js => true do
      page.should have_content('Reporting')
      page.should have_content('Content')
      page.should have_content('Dashboard')
    end

    context "Module submenu links" do
      scenario "See modules on submenu" do
        click_module_dropdown
        module_dropdown.should have_content('Publish')
        module_dropdown.should have_content('PayView')
        module_dropdown.should have_content('People')
      end

      scenario "See only active module on submenu" do
        module_dropdown.should have_content('Publish')
        module_dropdown.should_not have_content('PayView')
        module_dropdown.should_not have_content('People')
      end
    end

    context "Top navigation links for each module" do
      scenario "See links for publish module" do
        click_module_dropdown
        click_link 'Publish'
        header_menu.should_not have_content('People')
        header_menu.should have_content('Dashboard')
        header_menu.should have_content('Content')
        header_menu.should have_content('Reporting')
      end

      scenario "See links for People module" do
        click_module_dropdown
        click_link 'People'
        header_menu.should have_content('Dashboard')
        header_menu.should_not have_content('Content')
        header_menu.should have_content('Reporting')
        header_menu.should have_content('People')
      end

      scenario "See links for PayView module" do
        click_module_dropdown
        click_link 'PayView'
        header_menu.should have_content('Dashboard')
        header_menu.should have_content('Content')
        header_menu.should have_content('Reporting')
        header_menu.should_not have_content('People')
      end
    end

    context "Secondary menu" do

      scenario "Click user dropdown" do
        click_user_dropdown
        click_link "Settings"
        should_show_profile_page
      end

      scenario "Can see help center link"do

        click_user_dropdown
        find_link('Help Center').visible?.should be_true
      end

      scenario "Can see Logout center link" do
        click_user_dropdown
        find_link('Logout').visible?.should be_true
      end
    end
  end

  context "Show main content page elements" do
    context "Publish module" do
      before do
        visit_content(@user)
      end

      scenario "Should have upload button on left side" do
        content_topbar.should have_content("UPLOAD VIDEO")
      end
    end
  end


  context "Show links for video file per each module" do
    context "Publish module" do
      before do
        visit_content(@user,'publish', "entire_content/#{@video.id}")
      end

      context "Right content menus" do

        scenario "Should have top content links in order", :js => true do
          content_topbar.should have_content("Star")
          content_topbar.should have_content("Archive")
        end

        scenario "Should have distribution content menu" do
          content_sidebar.should have_content("DISTRIBUTION")
        end

        scenario "Should have NOTES content menu" do
          content_sidebar.should have_content("NOTES")
        end

        scenario "Should have SUBTITLES content menu" do
          content_sidebar.should have_content("SUBTITLES")
        end

        scenario "Should have DOWNLOADS content menu" do
          content_sidebar.should have_content("DOWNLOADS")
        end

        scenario "Should have METADATA content menu" do
          content_sidebar.should have_content("METADATA")
        end

        scenario "Should have GEOTARGETING content menu" do
          content_sidebar.should have_content("GEO-TARGETING")
        end
      end

      context "Left sidebar menu links" do
        scenario "Should have All content link" do
          sidebar.should have_content("All Content")
        end

        scenario "Should have Starred content link" do
          sidebar.should have_content("Starred Content")
        end

        scenario "Should have Archived content link" do
          sidebar.should have_content("Archived Content")
        end

        scenario "Should have Collections link" do
          sidebar.should have_content("COLLECTIONS")
        end
      end

    end

    context "PayView module" do
      before do
        visit_content(@user,'payview', "entire_content/#{@video.id}")
      end

      context "Right content menus" do

        scenario "Should have top content links in order", :js => true do
          content_topbar.should have_content("Star")
          content_topbar.should have_content("Archive")
        end

        scenario "Should have distribution content menu" do
          content_sidebar.should_not have_content("DISTRIBUTION")
        end

        scenario "Should have NOTES content menu" do
          content_sidebar.should_not have_content("NOTES")
        end

        scenario "Should have SUBTITLES content menu" do
          content_sidebar.should_not have_content("SUBTITLES")
        end

        scenario "Should have DOWNLOADS content menu" do
          content_sidebar.should_not have_content("DOWNLOADS")
        end

        scenario "Should have METADATA content menu" do
          content_sidebar.should_not have_content("METADATA")
        end

        scenario "Should have GEOTARGETING content menu" do
          content_sidebar.should_not have_content("GEO-TARGETING")
        end
      end

      context "Left sidebar menu links" do
        scenario "Should have All content link" do
          sidebar.should have_content("All Content")
        end

        scenario "Should have Starred content link" do
          sidebar.should have_content("Starred Content")
        end

        scenario "Should have Archived content link" do
          sidebar.should have_content("Archived Content")
        end

        scenario "Should have Collections link" do
          sidebar.should have_content("COLLECTIONS")
        end
      end

    end
  end
end
