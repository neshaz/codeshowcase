require 'spec_helper'

describe ContentOwner do
  describe "Associations" do
    it { should have_many(:content_owner_members) }
    it { should have_many(:users).through(:content_owner_members) }
    it { should have_many(:owner_module_members) }
    it { should have_many(:owner_modules).through(:owner_module_members) }
    it { should have_many(:videos) }
    it { should belong_to(:theme) }
  end

  describe "Attributes" do
    it { should allow_mass_assignment_of(:name) }
    it { should allow_mass_assignment_of(:description) }
    it { should allow_mass_assignment_of(:logo) }
    it { should allow_mass_assignment_of(:submodule) }
  end

  describe "Validations" do
    subject { FactoryGirl.create(:content_owner) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should validate_presence_of(:submodule) }
    it { should validate_uniqueness_of(:submodule) }
  end

  describe "Columns" do 
    it { should have_db_column(:nxe_cid).of_type(:string)}
    it { should have_db_column(:description).of_type(:text)}
    it { should have_db_column(:address).of_type(:string)}
    it { should have_db_column(:email).of_type(:string)}
    it { should have_db_column(:name).of_type(:string)}
    it { should have_db_column(:linkedin_address).of_type(:string)}
    it { should have_db_column(:videos_count).of_type(:integer)}
    it { should have_db_column(:theme_id).of_type(:integer)}
  end

end
