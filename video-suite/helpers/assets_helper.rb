module Archive::AssetsHelper
  def browse_count
    if !request.fullpath.include?('/orders')
      session[:assets_sidebar][:num_assets].to_s + " assets"
    else
      session[:orders_sidebar][:num_orders].to_s + " orders"
    end
  end

  def assoc_assets_count(master_asset)
    Asset.assoc_assets(master_asset).count
  end

  def assets_filter_active(current_assets_filter, page)
    if !current_assets_filter
      current_assets_filter = "master_assets"
    end
    active = (current_assets_filter == page) ? "active" : ""
    active
  end

  def assets_ribbon_class(asset_format)
    case asset_format
    when "JPG"
      rclass = "photo"
    when "PNG"
      rclass = "photo"
    when "JPEG"
      rclass = "photo"
    when "ProresHQ"
      rclass = "video"
    when "HDCAM"
      rclass = "video"
    when "MOV"
      rclass = "video"
    when "MPEG"
      rclass = "video"
    when "Master Package"
      rclass = "master"
    when "MP3"
      rclass = "sound"
    when "OGG"
      rclass = "sound"
    else
      rclass = "other"
    end
    rclass
  end

  def asset_title_view(asset)
    case asset.material_type
    when "Episodic"
      "#{asset.title} (Ep. #{asset.episode_number})"
    when "Trailer"
      "#{asset.title} (Trailer)"
    else
      asset.title
    end
  end

  def filename_view(file_name)
    case file_name
    when ""
      "No File Name"
    else
      file_name
    end
  end

  def filter_text
    filter = session[:assets_sidebar][:assets_view]
    case filter
    when "master_assets"
      rval = "Filter"
    when "all_assets"
      rval = "All"
    else
      rval = filter.capitalize
    end
    rval
  end

  def in_cart(asset)
    session[:shopping_cart].include?(asset.id)
  end

  def is_master(asset)
    asset.format == "Master Package"
  end

  def humanize_duration(duration)
    ndur = ""
    if duration == ":::"
      ndur = "-"
    else
      tarr = duration.split(':').map(&:to_i)
      tarr1 = []
      tarr.each do |t|
        if t < 10
          tarr1 << "0" + t.to_s
        else
          tarr1 << t.to_s
        end
      end
      ndur = tarr1.join(':')
    end
    ndur
  end

  def humanize_description(description)
    if description.include?("FileName:")
      ""
    else
      description.gsub(/\?s/,"'s")
    end
  end

  def li_classes(list, index)
    if list.length == 1
      "single"
    elsif index == 0
      "first"
    elsif index == (list.length - 1)
      "last"
    end
  end
end
