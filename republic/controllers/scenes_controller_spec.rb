require 'spec_helper'

describe ScenesController do
  describe "voting" do
    before do
      @instance = FactoryGirl.create(:scene)
      @params = {:project_id=>@instance.project, :id=>@instance, :category_id=>@instance.project.category}
    end

    it_should_behave_like("votable_controller")
  end

  describe "commenting" do
    before do
      user = FactoryGirl.create(:user)
      sign_in user
      @scene = FactoryGirl.create(:scene)
      @post = ->(args={}) { post(:create_comment, {:project_id=>@scene.project, :id=>@scene}.merge(args) ) } # 1 comment
    end

    it "should cache reply count for scenes" do
      @post.(typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
      @scene.comments_counter.should == 1
      @post.(typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
      @scene.comments_counter.should == 2
    end

    it "should cache reply count for comments" do
      @post.(typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
      @comment = Comment.last
      @post.(parent_id: @comment.id, typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
      @comment.comments_counter.should == 2
      @post.(parent_id: @comment.id, typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
      @comment.comments_counter.should == 3
      @scene.comments_counter.should == 3
    end

    it "should in include replies of replies in cache count" do
      @post.(typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
      @post.(typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
      @top_comment = @comment = Comment.last
      # create 2 subcomments for each @comment, rotating to new one.
      10.times do
        @post.(parent_id: @comment.id, typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
        @post.(parent_id: @comment.id, typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Lorem"})
        @comment = Comment.last
      end
      @top_comment.reload
      @top_comment.comments_counter.should == 3
      @scene.comments_counter.should == 22
    end

    it "should build comment tress" do
      #creating a large number of comments with subcomments with subcomments
      3.times do
        @post.(typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."})
        @parent = Comment.last
        5.times do
          @post.(parent_id: @parent.id ,typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Pellentesque habitant morbi tristique senectus et netus et malesuada fam"})
          @subcomment = Comment.last
          10.times do
             @post.(typeof: 'comments', 'type-sel' => 'comments', comment: {text: "Pellentesque habitant morbi tristique senectus"})
          end
        end
      end
      #retrieving the comment tree
    end

  end
end
