require 'spec_helper'

describe UsersHelper do
  include UsersHelper
  it "should test vote title" do
    user  = FactoryGirl.create :user
    scene = FactoryGirl.create :scene
    comment = FactoryGirl.create :comment, commentable: scene, text: "TESTVOTE"
    user.vote_for(comment)
    vote = Vote.last
    vote_title(vote).should == "Comment:"
    vote = user.vote_for(scene)
    vote_title(vote).should match /(Scene Suggestion:)/i
  end
end

describe User do
  it "should test user activity" do
    user = FactoryGirl.create(:user)
    FactoryGirl.create :comment, user: user
    act = user.activities.last
    act.name.should match /(Commented|On)/i
  end

  # Create at least 25 resources
  describe "should find user as eligible drafteer" do
    user = FactoryGirl.create(:user)
    p = Project.first||FactoryGirl.create(:project)
    10.times do
      scene = FactoryGirl.create(:scene, author: user, project: p)
      3.times do
        FactoryGirl.create(:comment, user: user, commentable: scene)
      end
    end
    user.reload

    User.eligible_drafteers.include?(user).should == true
    guild = FactoryGirl.create :guild
    user.guild!(guild)
    user.guild.should == guild
    User.eligible_drafteers.include?(user).should == false
  end

  describe "should not find user as eligible drafteer" do
    user = FactoryGirl.create(:user)
    User.eligible_drafteers.include?(user).should == false
    user.guild!(FactoryGirl.create(:guild))
    User.eligible_drafteers.include?(user).should == false
  end

  it "should test last week score update" do
    users = []
    10.times do
      u = FactoryGirl.create :user
      users << u
      # make user eligible, and each user actually will have 30 points (self upvote +1)
      30.times do
        FactoryGirl.create :comment, user: u
      end
    end
    users.each do |u|
      u.points.should == 30
      u.last_week_score.should == 0
    end
    User.update_last_week_score
    users.each do |u|
      u.reload
      u.last_week_score.should == 30
    end
  end
end

describe User do

  before(:each) do
    User.destroy_all("email = 'user@example.com'")
    @attr = {
      :name => "ExampleUser",
      :email => "user@example.com",
      :password => "foobar",
      :password_confirmation => "foobar"
    }
  end

  it "should create a new instance given a valid attribute" do
    User.create!(@attr)
  end

  it "should test aftercreate callback and ensure that user ranked" do
    f = User.create!(@attr)
    f.rank.should == Rank.first
  end

  it "should test rank up" do
    rank = Rank.first||FactoryGirl.create(:rank)
    f = User.create!(@attr.merge(rank_id: rank.id))
    ranks = Rank.where("id != #{rank.id} AND points > 0")
    ranks.each do |r|
      f.set_score_to(r.points-1)
      f.score(:up) # only this will trigger rank verification
      f.reload
      f.rank.should == r
    end
  end

  it "should require an email address" do
    no_email_user = User.new(@attr.merge(:email => ""))
    no_email_user.should_not be_valid
  end

  it "should accept valid email addresses" do
    addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    addresses.each do |address|
      valid_email_user = User.new(@attr.merge(:email => address))
      valid_email_user.should be_valid
    end
  end

  it "should reject invalid email addresses" do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
    addresses.each do |address|
      invalid_email_user = User.new(@attr.merge(:email => address))
      invalid_email_user.should_not be_valid
    end
  end

  it "should reject duplicate email addresses" do
    User.create!(@attr)
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end

  it "should reject email addresses identical up to case" do
    upcased_email = @attr[:email].upcase
    User.create!(@attr.merge(:email => upcased_email))
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end

  describe "passwords" do

    before(:each) do
      @user = User.new(@attr)
    end

    it "should have a password attribute" do
      @user.should respond_to(:password)
    end

    it "should have a password confirmation attribute" do
      @user.should respond_to(:password_confirmation)
    end
  end

  describe "password validations" do

    it "should require a password" do
      User.new(@attr.merge(:password => "", :password_confirmation => "")).
        should_not be_valid
    end

    it "should require a matching password confirmation" do
      User.new(@attr.merge(:password_confirmation => "invalid")).
        should_not be_valid
    end

    it "should reject short passwords" do
      short = "a" * 5
      hash = @attr.merge(:password => short, :password_confirmation => short)
      User.new(hash).should_not be_valid
    end

  end

  describe "password encryption" do
    before(:each) do
      @user = User.create!(@attr)
    end

    it "should have an encrypted password attribute" do
      @user.should respond_to(:encrypted_password)
    end

    it "should set the encrypted password attribute" do
      @user.encrypted_password.should_not be_blank
    end
  end

  it "should test points function" do
    @user = User.create!(@attr)
    @user.score.should == 0
    @user.score :up
    @user.score.should == 1
    @user.score :up
    @user.score :up
    @user.score.should == 3
    @user.score :down
    @user.score.should == 2
  end

  describe "Redis VS DB" do
    # see User#*_count methods
    #  approvals: 0,
    #  vetoes: 0,
    #  pitches: 0,
    #  comments: 0,
    #  following: 0,
    #  followers: 0,
    #  view_count: 0,
    #  scenes_score: 0,
    #  comments_score: 0

    before do
      @user = FactoryGirl.create :user
    end

    it "should test approvals" do
      FactoryGirl.create :vote, vote: true, voter: @user
      @user.stat(:approvals).should == @user.approvals_count
    end

    it "should test vetos" do
      FactoryGirl.create :vote, vote: false, voter: @user
      @user.stat(:vetoes).should == @user.vetoes_count
    end

    it "should test pitches" do
      FactoryGirl.create :scene, author: @user
      @user.stat(:pitches).should == @user.pitches_count
    end

    it "should test comments" do
      FactoryGirl.create :comment, user: @user
      @user.stat(:comments).should == @user.comments_count
    end

    it "should test followers" do
      follower = FactoryGirl.create :user
      follower.follow(@user)
      @user.stat(:followers).should == @user.followers_count
      follower.stop_following(@user)
      @user.stat(:followers).should == 0
    end

    it "should test following" do
      follower = FactoryGirl.create :user
      @user.follow(follower)
      @user.stat(:following).should == @user.following_count
      @user.stop_following(follower)
      @user.stat(:following).should == 0
    end

    it "should test view count" do
      visitor = FactoryGirl.create :user
      @user.incr_view_counter(visitor)
      @user.stat(:view_count).should == @user.view_count_count
    end

    it "should test scenes score count" do
      scene = FactoryGirl.create :scene, author: @user
      voter = FactoryGirl.create :user
      voter.vote_for(scene)
      @user.stat(:scenes_score).should == @user.scenes_score_count
    end

    it "should test comments score count" do
      comment = FactoryGirl.create :comment, user: @user
      voter = FactoryGirl.create :user
      voter.vote_for(comment)
      @user.stat(:comments_score).should == @user.comments_score_count
    end
  end

end