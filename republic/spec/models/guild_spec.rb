require 'spec_helper'

describe Guild do
  context "guild" do
    before do
      @guild = FactoryGirl.create :guild
      @user  = FactoryGirl.create :user
      @scene = FactoryGirl.create :scene, author: @user # random user will vote on @user scene
      @comment = FactoryGirl.create :comment, user: @user, commentable: @scene
      10.times do
      #  voting here. random users, @user scene
        FactoryGirl.create :vote, vote: false, voteable: @scene # guild will receive -11 to its score
      end
      ## pre-checks
      @user.guild_points.size.should  == 0
      @guild.guild_points.size.should == 0
      @user.received_votes.size.should == 12 # +1 for scene+comment upvote
      @user.guild!(@guild)
      @guild.reload
    end

    it "should test join guild, and then the stats mod's., then user the leave guild" do
      ## After-cloned checks, redis.
      @guild.users.count.should == 1
      @user.guild.should == @guild
      @guild.guild_points.size.should == 12
      @user.guild_points.size.should == 12 # check objects, +1 because of scene author auto upvote
      @guild.score.should == -8 # guild score should be affected by user score (votes>=(cloned)=>guild_points)
      @guild.stat(:comments).should == 1
      @guild.stat(:scenes).should == 1

      ## On the fly - activity checks
      cmn(@user) # add +1 guildpoint
      @guild.stat(:comments).should == 2
      FactoryGirl.create :scene, author: @user # add +1 guildpoint
      @guild.stat(:scenes).should == 2
      @guild.guild_points.count.should == 14

      ## User gained trophy for winning scene (SceneScript#move_to_phase2)
      phase2time = (CONFIG["script_phase2"]+1).hours
      phase3time = (CONFIG["script_phase3"]+1).hours
      p = @scene.project
      @now = Time.now # used to update back from timecop
      tkey = "#{@scene.category.key}_scene"
      scenetrophy = Trophy.get(tkey)||FactoryGirl.create(:trophy, key: tkey, name: tkey)
      ss = p.latest_ss
      ss.update_attribute :counter, 1 #skip premise
      SceneScript.cron_step2
      SceneScript.cron_step3
      Timecop.travel phase2time
      SceneScript.cron_step2
      SceneScript.cron_step3
      author = ss.reload.top_scene.author # "author" is actually @user
      author.user_trophies.size.should == 1 # should get one winning_scene
      author.has_trophy?(scenetrophy).should be true
      @guild.guild_points.count.should == 15 # trophy award 1 point
      @guild.stat(:win_scenes).should == 1

      ## Moving further with the time, ensuring things
      FactoryGirl.create :scene, author: @user # +1 guildpoint
      @guild.reload
      @guild.stat(:scenes).should == 3 # add +1 guildpoint
      @guild.guild_points.count.should == 16
      kill_comment = cmn(@user) # add +1 guildpoint # 16 here
      @guild.stat(:comments).should == 3
      kill_comment.destroy # also should remove 1 vote, 15 here
      @guild.stat(:comments).should == 2
      @guild.stat(:win_scenes).should == 1 # should not be affected

      ## Another user joining the guild
      @user2 = FactoryGirl.create :user
      5.times do
        cmn(@user2)
      end
      @user2.guild! @guild
      @guild.reload
      @user2.reload
      @guild.users.count.should == 2
      @guild.guild_points.count.should == 21
      scene = FactoryGirl.create :scene, author: @user2
      @guild.guild_points.count.should == 22
      @guild.stat(:comments).should == 7
      @guild.stat(:scenes).should == 4
      @guild.stat(:win_scenes).should == 1 # should not be affected
      FactoryGirl.create :vote, vote: true, voteable: scene # +1 guildpoints
      ## @user Leaving the guild, @user2 things should'nt be touched
      @user.unguild!
      @guild.reload
      @user.reload
      @user.guild_id.should be nil
      @guild.stat(:win_scenes).should == 0
      @guild.stat(:comments) == 5 # left from @user2
      @guild.stat(:scenes) == 2 # from @user2,
      @guild.users.count.should == 1
      @guild.guild_points.count.should == 7 # left from @user2 + anonymous vote
      ## Other users (not guild members) vote on guild resources (scenes or comments)
      # only @user2 left in the guild
      # TODO !!
    end
  end

  def cmn(u)
    FactoryGirl.create :comment, user: u
  end
  
end
