class User < ActiveRecord::Base
  include ActiveAdmin::Callbacks
  include Redis::Objects

  hash_key :stats

  acts_as_voter
  acts_as_follower
  acts_as_followable
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  apply_simple_captcha

  validates_presence_of :email
  validates_length_of :name, minimum: 4
  validates_uniqueness_of :name, :email
  validates_format_of :email, with: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i, message: "is malformed format"
  validates_format_of :name, with: /^[a-zA-Z0-9_\-\.]+$/i, message: "bad, can be alphanumeric, dash and underscore"
  validate :reserved_words


  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me,
                  :captcha, :captcha_key, :location

  
  geocoded_by :location
  after_validation :geocode, :if => :location_changed? 
  
  attr_accessor   :login
  attr_accessible :login

  has_one :user_config, dependent: :destroy
  accepts_nested_attributes_for :user_config
  attr_accessible :user_config_attributes

  has_many :avatars, dependent: :destroy
  has_many :scenes
  has_many :comments, dependent: :destroy
  belongs_to :rank
  has_and_belongs_to_many :roles
  has_many :guild_posts, dependent: :destroy
  has_many :guild_points, dependent: :destroy

  has_one :owned_guild, class_name: "Guild", foreign_key: "owner_id" # Owner of guild
  belongs_to :guild
  has_many :messages, foreign_key: "to_id", dependent: :destroy # get only incoming messages
  has_many :sent_messages, foreign_key: "from_id", dependent: :destroy, class_name: "Message"

  has_many :user_trophies, dependent: :destroy
  has_many :trophies, through: :user_trophies

  has_many :activities, dependent: :destroy
  # This is actually profile visit counter. to make counter available for specific time range (etc: last 30 days)
  has_many :readings, dependent: :destroy
  has_many :readings, as: :readable, dependent: :destroy

  # Received invitations from guilds
  has_many :guild_invitations, dependent: :destroy
  # Created invitation while in guild
  has_many :created_invitations, class_name: "GuildInvitation", foreign_key: "invited_by_id", dependent: :destroy

  # Used for few reports building
  attr_accessor :upvote_counts, :comment_counts

  acts_as_voter

  scope :recent, lambda { |limit| order("created_at DESC").limit(limit) }
  scope :with_role, lambda { |role_key| joins(:roles).where(["roles.key = ?", role_key]) }

  # Get most eligible drafteers - users not in the guild and having comments+scenes 25+
  scope :eligible_drafteers,
      joins("LEFT OUTER JOIN scenes ON scenes.user_id = users.id").
      joins("LEFT OUTER JOIN comments ON comments.user_id = users.id").
      joins("LEFT OUTER JOIN votes ON (scenes.id = votes.voteable_id AND votes.voteable_type = 'Scene') OR (comments.id = votes.voteable_id  AND votes.voteable_type = 'Comment')").
      where("users.guild_id IS NULL AND users.deleted_at IS NULL").
      group("users.id").
      order("( COUNT(comments.id) + COUNT(scenes.id) + COUNT(votes.vote = true) - COUNT(votes.vote = false) ) DESC")

  if Role.table_exists? # For first time migration
    for role in Role.all
      scope role.key.pluralize.to_sym, with_role(role.key)
    end
  end

  # Use that because ActiveAdmin override default callback
  before_save do
    self.url = name.urlize
  end

  #assigns rank after user is created
  after_create do
    stats_assign
    grant_rank(Rank.first)
  end

  def soft_delete
    comments.each do |c|
      c.soft_delete
    end
    votes.each do |v|
      v.soft_delete
    end
    update_attribute(:deleted_at, DateTime.now)
  end

  def display_with_avatar
    if avatars.active.first
      "<img class='auto_image' src='#{avatars.active.first.img(:tiny)}'/><span class='auto_name'>#{name}</span>".html_safe
    else
      "<img class='auto_image' src='/no_avatar/tiny/missing.png'/><span class='auto_name'>#{name}</span>".html_safe
    end
  end

  def active_for_authentication?
    super && !deleted_at
  end

  def inactive_message
    !!deleted_at ? :deleted : super
  end

  # Check if user configured for key, key should come from .user_configs
  def configured_for?(key)
    return false if CONFIG["disable_user_notifications"]
    user_config.send(key)
  end

  def win_pitches
    user_trophies.for_scene.where(awardable_id: scene_ids)
  end

  def ppp
    return 0 if points == 0
    (stat(:pitches).to_f / points.to_f).round(2)
  end

  def ranged_score(from=5.years.ago, till=nil)
    t = till||@now||Time.now
    votes.select("SUM(vote) AS sum").in_range(from, t).where(["voteable_id IN (?)", res_ids]).map(&:sum).sum.to_i
  end

  # Return average score("vote") per scene.
  def avg_per_pitch(from=5.years.ago, till=nil)
    t = till||@now||Time.now
    ranged_scenes = scenes.in_range(from, t)
    v_count = votes.for_scene.select("SUM(vote) AS sum").in_range(from, t).where(["voteable_id IN (?)", ranged_scenes.map(&:id)]).map(&:sum).sum.to_i
    s_count = ranged_scenes.count
    s_count > 0 ? ((v_count.to_f / s_count.to_f).round(1)) : 0.0
  end

  def score_from_global(from=5.years.ago, till=nil)
    t = till||@now||Time.now
    uvotes = votes.select("SUM(vote) AS sum").in_range(from, t).where(["voteable_id IN (?)", res_ids]).map(&:sum).sum.to_i
    global = Vote.upvotes.count
    ((uvotes.to_f / global)*100).round.to_s + "%"
  end

  def res_ids
    scene_ids + comment_ids
  end
  #def last_week_rank
  #  @now ||= Time.now
  #  return rank if last_rank_at.nil?
  #  @a_ranks ||= Rank.all
  #  r = @a_ranks.index(rank) - 1
  #  r < 0 ? rank : @a_ranks[r]
  #end

  def grant_rank(next_rank)
    transaction do
      self.rank = next_rank
      self.last_rank_at = Time.now
      save
      activities.create activable: next_rank, type_of: :rankup
      if next_rank != Rank.first
        #will not notify of rank up is equal to Intern
        Email.notify_user(self, next_rank, :n_rank_up).deliver
      end
    end
  end

  # Login by :name or :email, "login" is attr_accessible
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(name) = :value OR lower(email) = :value", {value: login.downcase }]).first
    else
      where(conditions).first
    end
  end

  # Providing backup for old "avatar" method.
  # Return active avatar or any
  def avatar
    avatars.active.first||avatars.first
  end

  # this also should create message to user (guild_invitation callback)
  def invite_to_guild(user, text)
    raise "User id##{id} does not belongs to any guild to invite someone" unless guild
    raise "User id##{user.id} already belongs to guild ##{user.guild_id}" if user.guild
    created_invitations.create user: user, guild: guild, invitation_text: text
  end

  # Get real scenes score, that received for this user scenes
  def scenes_score
    Vote.for_scene.select("SUM(vote) AS sum").where(["voteable_id IN (?)", scene_ids]).map(&:sum).sum.to_i
  end

  #def show_user_with_avatar
  #  (avatar ? "<img src='#{avatar.img.url(:small)}' /> ".html_safe : "") + name
  #end

  ## those methods used to reload user redis cache and ranged for guild points
  # Get real comments score, that received for this user comments
  def comments_score
    Vote.for_comment.select("SUM(vote) AS sum").where(["voteable_id IN (?)", comment_ids]).map(&:sum).sum.to_i
  end

  # Votes that received for this user created scenes and comments (did by other users)
  def received_votes(select="*")
    Vote.for_scene.where(["voteable_id IN (?)", scene_ids]).select(select) + Vote.for_comment.where(["voteable_id IN (?)", comment_ids]).select(select)
  end

  # Join guild.
  # (+) Add guildpoints to the guild:
  #-> Received approve/veto votes,
  #-> Received winning scene/premise trophy
  # (-) It should not add guildpoints:
  #-> from user: votes, comments, scenes
  #-> any other trophy than above
  def guild!(guild)
    raise "Guild#id is nil when it should'nt or the guild record is not persistent" if guild.nil? || guild.new_record?
    now = Time.now
    transaction do
      update_attribute(:guild_id, guild.id)
      received_votes("vote, voteable_type").each do |vote|
        guild_points.create! guild: guild, points: (vote.vote ? 1 : -1), voted_at: now,
                            source_cd: GuildPoint.sources[vote.voteable_type]
      end
      guild.award_winning_scenes(self)
      guild.stats.incr :comments, comments_count
      guild.stats.incr :scenes, scenes.count
      activities.create activable: guild, type_of: :guildin
    end
  end

  # Remove from the guild and remove guild points
  def unguild!
    raise "User not in any guild" unless guild
    reload
    transaction do
      activities.create activable: guild, type_of: :guildout
      guild.reduce_winning_scenes(self)
      guild.stats.incr :comments, -comments_count
      guild.stats.incr :scenes, -scenes.count
      guild_points.destroy_all
      update_attribute(:guild_id, nil)
    end
  end

  def reserved_words
    words = CONFIG["reserved_words"]
    errors.add(:name, "&#147;#{name}&#148; is a reserved word. Please try another.") if words.include?(name.to_s.downcase)
  end

  def send_message_from(from, message)
    # see enum_as method in Messaging
    source = from.is_a?(Guild) ? 1 : 0
    messages.create(text: message, from_id: from.id, source_cd: source)
  end

  def in_guild?
    guild.present?
  end

  # Default for graphs
  def upvote_counts
    @upvote_counts || 0
  end

  # Default for graphs
  def comment_counts
    @comment_counts || stat(:comments)
  end

  ## BEGIN redis stuff
  # Dont use @user.stats[***] directly.
  # If in some case REDIS data missing this will lead to "" value
  # So in case that value are blank (not numerical)
  def stats_assign # factory does not use after_create
    stats.bulk_set(
      approvals: 0,
      vetoes: 0,
      pitches: 0,
      comments: 0,
      following: 0,
      followers: 0,
      view_count: 0,
      scenes_score: 0,
      comments_score: 0
    )
    # !! Any new stat should be added to user model spec !
  end

  ### REDIS cache
  # those *_count methods used to return real value from db to help redis syncronize data
  # methods and keys of bulk_set should match
  def approvals_count
    votes.upvotes.count
  end

  def vetoes_count
    votes.downvotes.count
  end

  def pitches_count
    scenes.count
  end

  def comments_count
    comments.count
  end

  def following_count
    follows.count
  end

  def followers_count
    followers.count
  end

  # Scenes score based on live db voting for scenes (sum of +/-)
  def scenes_score_count
    scenes_score
  end

  # Comments score based on live db voting for comments (sum of +/-)
  def comments_score_count
    comments_score
  end

  # Reason for that stupid name is convention of use in counter method
  def view_count_count
    #view_count TODO remove that column from db
    readings.count
  end
  ### END redis stuff

  # Dup into redis also. In future production dont use this DB field
  # Counter read from redis only.
  def incr_view_counter(user)
    readings.create(user_id: user ? user.id : nil)
    stats.incr(:view_count, 1)
  end

  # Use redis. can be switched to db
  def profile_views
    stat(:view_count)
  end

  # returns [user_id, number of votes] And [user_id, number of scene comments]
  def fans
    my_scenes = scene_ids
    my_comments = comment_ids
    votes = Vote.arel_table
    my_voters = Vote.
        where(:vote => true).
        where(
        votes[:voteable_type].eq('Scene').and(votes[:voteable_id].in(my_scenes)).or(
            votes[:voteable_type].eq('Comment').and(votes[:voteable_id].in(my_comments))
        ).and(
            votes[:vote]
        )
    ).group(:voter_id).
        select('votes.voter_id, Count(votes.vote) as votes').
        order('votes').
        all.map { |v| [v.voter_id, v.votes] }

    comms = Comment.arel_table
    my_commentors = Comment.where(commentable_type: 'Scene', commentable_id: my_scenes).
        group(:user_id).
        select('comments.user_id, Count(comments.id) as comms').
        order('comms').all.map { |c| [c.user_id, c.comms] }
    [my_voters, my_commentors]
  end

  # Get user scores (from points attributes)
  # :up|:down used to add/reduce user a point
  #  -> according to voteable (should be scene or comment object - to know to which **_score do the change)
  def score(way=false, voteable=false)
    return points unless way
    add = way == :up ? 1 : -1
    # next row used for stubbing some tests which not really testing score relation
    from = voteable ? voteable.score_var : :comments_score
    stats.incr from, add
    snew = points # reread TOTAL score (comments+scenes)
    if way == :up # dont need to check eligibility if downed
      next_rank = Rank.where(["points <= ? AND id > ?", snew , rank_id]).first
      grant_rank(next_rank) if next_rank
    end
    snew
  end

  # Set redis score. should be used in tests only
  def set_score_to(score)
    stats.incr(:comments_score, score-points)
  end

  def points
    stat(:scenes_score) + stat(:comments_score)
  end

  # Use url unstead of ID in routing
  def to_param
    url
  end

  def has_role?(role)
    loaded_roles.include?(Role.find_by_key(role.to_s))
  end

  # load once during all instance
  def loaded_roles
    @loaded_roles ||= roles
  end

  def primary_role
    roles.first
  end

  def self.get(url)
    find_by_url(url)||raise(ActiveRecord::RecordNotFound)
  end

  def award_trophies
    existing = user_trophies
    avail = Trophy.without(existing.map(&:trophy))
    awarded = []
    avail.each do |trophy|
      if Eligible.for(self, trophy) && !awarded.include?(trophy) # exclude already awarded during thin runtime
        self.award(trophy)
        awarded << trophy
      end
    end
    awarded
  end

  # Award trophy by user. this function simply award without eligibility check.
  def award(trophy_or_key, by_user=nil, for_object = nil)
    t = trophy_or_key.is_a?(String) ? Trophy.find_by_key(trophy_or_key) : trophy_or_key
    return false if has_trophy?(t) # dont award twice !
    res = false
    transaction do
      get_it = user_trophies.new trophy: t, awarded_by_user: by_user
      get_it.save
      res = get_it.new_record? ? false : get_it # awarded or not ?
      # Assign user_trophy to polymorphic object. so u can find fow what object this award received
      if res && for_object
        get_it.awardable = for_object
        get_it.save
      end
      Email.notify_user(self, res, :n_get_achievement).deliver if res
    end
    res
  end

  def has_trophy?(trophy)
    user_trophies.map(&:trophy_id).include?(trophy.id)
  end

  def text
    ""
  end

  # Run cron to update on every monday, update last week score for users
  def self.update_last_week_score
    c = 0
    User.find_in_batches do |group|
      group.each do |user|
        user.update_attribute :last_week_score, user.scenes_score + user.comments_score
        c += 1
      end
    end
    logger.info "CRON:: update_last_week_score::#{c}"
  end

  private
end
