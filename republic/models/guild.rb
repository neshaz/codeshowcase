class Guild < ActiveRecord::Base
  include ActiveAdmin::Callbacks
  include Redis::Objects

  hash_key :stats

  validates_presence_of :name, :description
  validates_length_of :description, maximum: 56
  validates_length_of :name, in: 3..50

  has_attached_file :logo, styles: {medium: "82x76#", small: "32x32#", big: "346x262>"},
                    storage: $dev ? :filesystem : :s3,
                    s3_credentials: File.join(Rails.root, "config/s3.yml")

  validates_attachment_content_type :logo, content_type: ["image/png", "image/gif", "image/jpg", "image/jpeg"]
  validates_attachment_size :logo, less_than: 1025.kilobytes

  validate :reserved_words

  has_many :users
  has_many :scenes, through: :users
  has_many :comments, through: :users
  belongs_to :owner, foreign_key: "owner_id", class_name: "User"
  has_many :guild_invitations, dependent: :destroy
  has_many :guild_posts, dependent: :destroy

  has_many :guild_points, dependent: :destroy

  scope :active, where(closed: false, tmp: false)
  scope :in_range, lambda {|past, future=nil| where("guilds.created_at BETWEEN ? AND ?", past, future||Time.now) }
  # This scope will return sorted array by guild rank.
  # Since its using LEFT JOIN, if the Guild has no guild_points it will not be included in reply, actually
  # -> this should not happen. (user who create the guild and join it will have at least some points)
  scope :ranked, lambda {|past=nil, future=nil| joins("LEFT JOIN guild_points ON guild_points.guild_id = guilds.id").
            group("guilds.id").order("sum DESC").
            where(["guild_points.created_at BETWEEN ? AND ?", past||5.years.ago, future||Time.now]).
            select("guilds.*, SUM(guild_points.points) AS sum")
  }
  scope :unranked, joins("LEFT OUTER JOIN guild_points ON guild_points.guild_id = guilds.id").
            group("guilds.id").order("sum DESC, guilds.id ASC").
            where("guild_points.id IS NULL").
            select("guilds.*, SUM(guild_points.points) AS sum")

  before_create do
    self.url = name.urlize
  end

  after_create do
    stats_assign
  end

  # Global guild rank
  # ??? merge with ranged ?
  # parted means return array like [1, "th"] to use in guilds#show action
  def guild_rank(parted = false)
    guilds = Guild.ranked
    r = guilds.index(self)
    r ? (parted ? [r+1, (r+1).ordinalize.gsub(/\d/, "")]: (r+1).ordinalize) : "Last"
  end

  def text
    name
  end

  # Measured guild rank by timerange (week/month/year)
  # parted arg means if return array with [place_int, ordinalize_str] or "Last" for action "show
  def ranged_rank(timeago, parted = false)
    @now ||= Time.now
    guilds = Guild.ranked(timeago, @now)
    guilds += Guild.unranked
    r = guilds.index(self)
    if r
      if r+1 < guilds.count
        place = r+1
        reply = (parted ? [place, place.ordinalize.gsub(/\d/, "")]: place.ordinalize)
        parted ? [place, place.ordinalize.gsub(/\d/, "")] : [place, reply]
      else
        reply = "Last"
        place = 999999
        parted ? reply : [place, reply]
      end
    else
      reply = "Last"
      place = 999999
      parted ? reply : [place, reply]
    end
  end

  # Read from db TODO to redis, "point_count"
  def score(from=5.years.ago, till=nil)
    t = till||@now||Time.now
    guild_points.in_range(from, t).summarize
    # old
    #scene_score(from, till) + comment_score(from,till)
  end

  # Scene score from all current guild users
  def scene_score(from=5.years.ago, till=nil)
    t = till||@now||Time.now
    guild_points.for_scene.in_range(from, t).summarize
    # get from users. old method.
    #Vote.for_scene.in_range(from, till).select("SUM(vote) AS sum").where(["voteable_id IN (?)", scene_ids]).map(&:sum).sum.to_i
  end

  # Comments score from all current guild users
  def comment_score(from=5.years.ago, till=nil)
    t = till||@now||Time.now
    guild_points.for_comment.in_range(from, t).summarize
    # get from users. old method.
    #Vote.for_scene.in_range(from, till).select("SUM(vote) AS sum").where(["voteable_id IN (?)", comment_ids]).map(&:sum).sum.to_i
  end

  def reserved_words
    words = CONFIG["reserved_words"]
    errors.add_to_base("#{name} is a reserved word. Please try another.") if words.include?(name.to_s.downcase)
  end

  def stats_assign # factory does not use after_create
    stats.bulk_set(win_scenes: 0, comments: 0, points: 0 )
  end

  def to_param
    url
  end

  def self.get(url)
    find_by_url(url)||raise(ActiveRecord::RecordNotFound)
  end

  ## BEGIN redis count, db fallback when redis data lost or unavailable
  # see keys on stats_assign. those methods should with _count.
  # also see core_extention of AR -> stat method
  def win_scenes_count
    guild_points.for_trophy.count
  end

  def scenes_count
    scenes.count
  end

  def points_count
    score
  end

  def comments_count
    comments.count
  end
  ## END

  def invited?(user)
    !guild_invitations.of_user(user).blank?
  end

  def invitation_of(user)
    guild_invitations.of_user(user).first
  end

  # Cronjob
  def self.remove_temporary
    g = Guild.where(["tmp = 1 AND created_at + INTERVAL 1 DAY <= NOW()"])
    logger.info ">> Destroying #{g.count} temporary guilds"
    g.destroy_all
  end

  def activate!
    self.tmp = false
    self.closed = false
    save
  end

  def close!
    self.closed = true
    save!
  end

  # Add scores to "win_scenes"
  # "once" argument means add only 1 point (used in SceneScript#move_to_phase2) to prevent same trophies add win_scenes
  def award_winning_scenes(from_user, once=false)
    eligible = once ? 1 : from_user.trophies.guild_awardable.count
    #puts "$$ G-AWARD #{id}:#{name} by user:#{from_user.name} for '#{eligible}' points"
    eligible.times do
      guild_points.create source: "Trophy", user: from_user
    end
    stats.incr :win_scenes, eligible
  end

  # reduce redis win_scens, guild_points will be destroyed in the User#unguild!
  def reduce_winning_scenes(user) # leaving user
    eligible = user.trophies.guild_awardable.count
    stats.incr :win_scenes, -eligible
  end
end
