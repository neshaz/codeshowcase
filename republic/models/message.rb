class Message < ActiveRecord::Base

  validates_presence_of :text, :to_id, :from_id

  # private: user to user
  # guild: user to guild members
  # invitation: invitation to user
  # admin: admin to user+s
  # voting: user to guild members, set when user approve guild invitation and members can start vote after this msg
  # system: may be used for broadcast from super admin or some system notification/changes, also for auto-replies of invs
  ## Also all those keys should have partials in the messages/source folder even if no use.
  as_enum :source, private: 0, guild: 1, invitation: 2, admin: 3, voting: 4, system: 9

  belongs_to :sender, class_name: "User", foreign_key: "from_id"
  belongs_to :recipient, class_name: "User", foreign_key: "to_id"
  belongs_to :guild_invitation

  scope :unread, where(readed: false)
  scope :read, where(readed: true)
  scope :latest, order("messages.created_at DESC")
  scope :for_user, lambda {|u| where(to_id: u.id) }
  scope :from_user, lambda {|u| where(from_id: u.id) }
  scope :from_or_to, lambda {|u1,u2| where(["(from_id = ? AND to_id = ?) OR (to_id = ? AND from_id = ?)", u1.id, u2.id,u1.id, u2.id]) }
  scope :any_message_for, lambda {|u| where("from_id = ? OR to_id = ?", u.id, u.id)}
  scope :with_text_or_subject, lambda {|text| where(["LOWER(text) LIKE ? OR LOWER(subject) LIKE ?", "%#{text.downcase}%", "%#{text.downcase}%"]).latest }

  # Send message from user to user with text, and source.
  # source can be number (according to source) or key
  # inv_id is the guild_invitation_id. required when using some source/partials
  def self.send_message(from, to, subject, text, source=0, inv_id = nil)
    scd = source.is_a?(Integer) ? source : Message.sources[source]
    msg = create text: text, sender: from, recipient: to, source_cd: scd, subject: subject, guild_invitation_id: inv_id
    Email.notify_user(to, msg, :n_receive_mess).deliver
    msg
  end

  def outbox_of?(user)
    from_id == user.id
  end

  def readed!
    update_attribute(:readed, true) unless readed?
  end

  def sent_at
    created_at.strftime("%d/%b/%Y")
  end

  def toggle_readed!
    update_attribute(:readed, !readed?)
  end

end
