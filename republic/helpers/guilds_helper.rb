module GuildsHelper

  # Return ordinalized rank from
  def weekly_rank(users, user)
    # Add +1 because of index start from 0, multiply by current page (to reflect real position)
    pos = (users.index(user) + 1) + (20*(users.current_page-1))
    pos.ordinalize
  end

  # Percent =)
  def percent(a, b, with_sign="%")
    return "0#{with_sign}" if b==0
    "#{(a.to_f/b.to_f*100).to_i}#{with_sign}"
  end

  # average Points-Per-Pitch
  def ppp(guild)
    return 0 if guild.stat(:points) == 0
    (guild.stat(:scenes).to_f / guild.stat(:points).to_f).round(2)
  end

  def brainstorm?
    action_name == "brainstorm"
  end

  # Return ALL points in ranged time
  def points_per_pitch(guild, timeago)
    score   = guild.score(timeago).to_f
    pitches = guild.scenes.in_range(timeago).count
    comments= guild.comments.in_range(timeago).count
    t = (pitches + comments).to_f
    return 0 if t == 0.0 || score == 0.0
    (t / score).round(1)
  end

  def options_for_select_with_data(collection, value_method, text_method, selected = nil, data = {})
    options = collection.map do |element|
      [element.send(text_method), element.send(value_method), data.map do |k, v|
        {"data-#{k}" => element.send(v)}
      end
      ].flatten
    end
    selected, disabled = extract_selected_and_disabled(selected)
    select_deselect = {}
    select_deselect[:selected] = extract_values_from_collection(collection, value_method, selected)
    select_deselect[:disabled] = extract_values_from_collection(collection, value_method, disabled)
    options_for_select(options, select_deselect)
  end

end
