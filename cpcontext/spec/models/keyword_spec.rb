require 'spec_helper'

describe Keyword do
  
  describe "shared functionality" do
    before(:each) { @class = Keyword }
    it_should_behave_like "named model"
    it_should_behave_like "indexed model"
    it_should_behave_like "work item"
  end
  
  it { should have_and_belong_to_many(:texts) }
  
  describe "sector field" do
    it "should default to false" do
      Fabricate(:keyword).sector.in?([true,false]).should be_true
    end
  end
  
  describe "scope" do
    before(@each) do
      @s1 = Fabricate(:keyword, sector: true)
      @s2 = Fabricate(:keyword, sector: true)
      @t3 = Fabricate(:keyword, sector: false)
    end
    describe "sectors" do
      it "should return sector keywords" do
        Keyword.sectors.to_a.sort.should == [@s1,@s2].sort
      end
    end
    describe "normal" do
      it "should return non-sector keywords" do
        Keyword.normal.to_a.should == [@t3]
      end
    end
  end

  describe "derived fields" do
    
    describe "#date" do
      it "should be the date last updated" do
        updated_at = "2111-11-11".to_time
        k = Fabricate.build(:keyword, updated_at: updated_at)
        k.date.should == updated_at.to_date
      end
    end
    describe "#overall_value_rating" do
      it "should be the average overall_value_rating of related texts, rounded to an integer" do
        k = Fabricate(:keyword, sector: true)
        Fabricate(:text, keywords: [k], overall_value_rating: 5)
        Fabricate(:text, keywords: [k], overall_value_rating: 2)
        Fabricate(:text, keywords: [k], overall_value_rating: 3)
        k.reload
        average = [5,2,3].mean.round
        k.overall_value_rating.should == average
      end
      it "should be nil if there are no related texts" do
        Fabricate.build(:keyword).overall_value_rating.should be_nil
      end
    end
    describe "#keyword_ids" do
      it "should just be this person's own id" do
        k = Fabricate(:keyword)
        k.keyword_ids.should == [k.id]
      end
    end
    describe "#person_ids" do
      it "should be the union of person_ids from related texts" do
        a1, a2, a3 = Fabricate(:person), Fabricate(:person), Fabricate(:person)
        d1, d2 = Fabricate(:text, people: [a1,a2]), Fabricate(:text, people: [a2,a3])
        k = Fabricate(:keyword, texts: [d1,d2])
        k.person_ids.should == [a1,a2,a3].map(&:id)
      end
    end
    describe "#source_ids" do
      it "should be the union of source_ids from related texts" do
        s1, s2, s3 = Fabricate(:source), Fabricate(:source), Fabricate(:source)
        d1, d2 = Fabricate(:text, sources: [s1,s2]), Fabricate(:text, sources: [s2,s3])
        k = Fabricate(:keyword, texts: [d1,d2])
        k.source_ids.should == [s1,s2,s3].map(&:id)
      end
    end
    
  end

  describe "reindexing of texts" do
    before (:each) do
      @keyword = Fabricate(:keyword, sector: true)
      text = Fabricate(:text, keywords: [@keyword])
      @keyword.reload
      new_text = double("new text")
      Text.should_receive(:find).with(text.id).and_return(new_text)
      new_text.should_receive(:update_index).once
    end
    it "should happen when the keyword is saved" do
      @keyword.save
    end
    it "should happen when the keyword is destroyed" do
      @keyword.destroy
    end
    it "should happen when the text is disassociated with the keyword" do
      @keyword.texts = []
      @keyword.save
    end
  end

end
