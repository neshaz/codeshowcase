require 'spec_helper'

describe Person do
  
  describe "shared functionality" do
    before(:each) { @class = Person }
    it_should_behave_like "named model"
    it_should_behave_like "indexed model"
    it_should_behave_like "work item"
  end
    
  it { should have_and_belong_to_many(:texts) }
  
  describe "#name" do
    it "should include Chinese name in correct order" do
      p = Fabricate.build(:person)
      p.name.should match(/#{p.family_name_cn + p.given_name_cn}/)
    end
    it "should upcase English family name and follow it with given name" do
      p = Fabricate.build(:person)
      p.name.should match(/#{p.family_name_en.upcase} #{p.given_name_en}/)
    end
  end
  
  it "should accept a photo as JPG, PNG, GIF, and no other files" do
    {
      "face.jpg"  => true,
      "face.png"  => true,
      "face.gif"  => true,
      "ascii.txt" => false,
      "empty.nil" => false
    }.each do |file, file_okay|
      person = Fabricate(:person)
      person.photo = File.new("#{Rails.root}/spec/support/resources/#{file}")
      person.save
      person.errors_on(:photo).should     be_empty if     file_okay
      person.errors_on(:photo).should_not be_empty if not file_okay
    end
  end
  
  describe "derived fields" do
    
    describe "#date" do
      it "should be the date last updated" do
        updated_at = "2111-11-11".to_time
        a = Fabricate.build(:person, updated_at: updated_at)
        a.date.should == updated_at.to_date
      end
    end
    describe "#overall_value_rating" do
      it "should be the average overall_value_rating of related texts, rounded to an integer" do
        a = Fabricate(:person)
        Fabricate(:text, people: [a], overall_value_rating: 5)
        Fabricate(:text, people: [a], overall_value_rating: 2)
        Fabricate(:text, people: [a], overall_value_rating: 3)
        a.reload
        average = [5,2,3].mean.round
        a.overall_value_rating.should == average
      end
      it "should be nil if there are no related texts" do
        Fabricate.build(:person).overall_value_rating.should be_nil
      end
    end
    describe "#person_ids" do
      it "should just be this person's own id" do
        a = Fabricate(:person)
        a.person_ids.should == [a.id]
      end
    end
    describe "#keyword_ids" do
      it "should be the union of keyword_ids from related texts" do
        k1, k2, k3 = Fabricate(:keyword), Fabricate(:keyword, sector: true), Fabricate(:keyword)
        d1, d2 = Fabricate(:text, keywords: [k1,k2]), Fabricate(:text, keywords: [k2,k3])
        a = Fabricate(:person, texts: [d1,d2])
        a.keyword_ids.should == [k1,k2,k3].map(&:id)
      end
    end
    describe "#source_ids" do
      it "should be the union of source_ids from related texts" do
        s1, s2, s3 = Fabricate(:source), Fabricate(:source), Fabricate(:source)
        d1, d2 = Fabricate(:text, sources: [s1,s2]), Fabricate(:text, sources: [s2,s3])
        a = Fabricate(:person, texts: [d1,d2])
        a.source_ids.should == [s1,s2,s3].map(&:id)
      end
    end
    
  end
  
  describe "reindexing of texts" do
    before (:each) do
      text = Fabricate(:text)
      @person = Fabricate(:person, texts: [text])
      new_text = double("new text")
      Text.should_receive(:find).with(text.id).and_return(new_text)
      new_text.should_receive(:update_index).once
    end
    it "should happen when the person is saved" do
      @person.save
    end
    it "should happen when the person is destroyed" do
      @person.destroy
    end
    it "should happen when the peoplehip relationship ends" do
      @person.texts = []
      @person.save
    end
  end
  
end
