require 'spec_helper'

describe "People" do
  include Support::AuthenticationMacros
  before(:each) { @user, @client = create_and_sign_in_user_with_client('admin') }

  def valid_attributes
    { clients_tokens: [@client.id.to_s] }
  end

  it "should show an individual person" do
    person = Fabricate(:person, valid_attributes)
    visit url_for(person)
    page.status_code.should == 200
    page.should have_content(person.name_en)
  end

  describe "editing" do
    
    it "should provide a form to edit an individual" do
      person = Fabricate(:person)
      visit edit_person_path(person)
      page.status_code.should == 200
      page.should have_css("form")
    end

    describe "photos" do
      before(:each) do
        @person = Fabricate(:person)
        visit edit_person_path(@person)
      end
      
      it "should have a file upload field for a photo" do
        page.should have_xpath("//input[@id='person_photo'][@type='file']")
      end

      xit "should store an uploaded photo and show it on the show page" do
        attach_file("person_photo", "#{Rails.root}/spec/support/resources/face.jpg")
        click_on "Update"
        visit person_path(@person)
        page.should have_xpath("//img[@alt=\"#{@person.name_en}\"]")
      end

      xit "should show any existing photo on the edit page" do
        attach_file("person_photo", "#{Rails.root}/spec/support/resources/face.jpg")
        click_on "Update"
        visit edit_person_path(@person)
        page.should have_xpath("//img[@alt=\"#{@person.name_en}\"]")
      end

      xit "should show allow removal of a photo" do
        attach_file("person_photo", "#{Rails.root}/spec/support/resources/face.jpg")
        click_on "Update"
        visit edit_person_path(@person)
        check :person_remove_photo
        click_on "Update Person"
        page.should_not have_xpath("//img[@alt=\"#{@person.name_en}\"]")
      end
    end
    
  end

end
