class PeopleController < InheritedResources::Base
  include ControllerHelper
  load_and_authorize_resource :except => :restore
  before_filter :previous_query!, :only => :show
  before_filter :get_type_history!, :only => :edit
  before_filter :mark_updated_by!, :only => [:create, :update]
  before_filter :find_soft_deleted_person!, :only => :restore
  before_filter :set_empty_search, :only => :show

  layout :compute_layout

  def show
    respond_to do |format|
      format.js
      format.html { render :layout => "application-static" }
    end
  end

  def index
    results = TokenInput::Query.new(model: Person, q: params[:q]).results
    respond_to do |format|
      format.json { render text: results.to_tokeninput_json }
      format.html { redirect_to people_path } # private method bellow
    end
  end
  
  def create
    create! do |success, failure|
      success.html { redirect_to person_path(resource) }
      failure.html { render :new }
    end
  end

  def update
    update! do |success, failure|
      success.html { redirect_to person_path(resource) }
      failure.html { render :edit }
    end
  end
  
  def destroy
    resource.destroy
    respond_to do |format|
      format.html { redirect_to people_path, :notice => "Person was successfully deleted." }
      format.js
    end
  end

  def restore
    @person.restore
    @person.reindex!
    respond_to do |format|
      format.html { redirect_to people_path, :notice => "Person was successfully restored." }
      format.js
    end
  end

  private
  
  def find_soft_deleted_person!
    @person = Person.deleted.find(params[:id])
  end
  
  def people_path
    url_for(SearchQuery.new.add(SearchParameter::Type.new('person')).p1.params)
  end

  def compute_layout
    action_name == "show" ? "application-new" : "application"
  end
end
