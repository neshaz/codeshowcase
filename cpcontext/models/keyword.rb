# encoding: UTF-8
class Keyword
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia

  include TokenInput::HelpersForSelector
  extend  TokenInput::HelpersForSelected
  
  include ModelConcerns::Tire
  include ModelConcerns::SearchParameter
  include ModelConcerns::WorkItem
  include ModelConcerns::Reindex
  include ModelConcerns::Duplicate
  include ModelConcerns::UpdateConflict
  include ModelConcerns::Destroy
  
  field :sector, type: Boolean, default: false

  field :name_en, type: String
  field :name_cn, type: String

  field :picture_url, :type => String

  field :updated_by, type: BSON::ObjectId

  has_and_belongs_to_many :texts
  after_save :reindex_texts
  after_destroy :reindex_texts
  def texts=(new_texts)
    self.text_ids = new_texts.map(&:id)
  end
  
  has_and_belongs_to_many :clients
  after_save :reindex_clients
  def clients=(new_clients)
    self.client_ids = new_clients.map(&:id)
  end
  attr_checkbox_tokeninput :clients
  
  has_and_belongs_to_many :debates
  after_save :reindex_debates
  def debates=(new_debates)
    self.debate_ids = new_debates.map(&:id)
  end
  
  validates_presence_of :clients
  validate :handle_conflict, :only => :update
  #validates_uniqueness_of :name_en, :unless => :allow_duplicate
  #validates_uniqueness_of :name_cn, :unless => :allow_duplicate
  
  scope :sectors, where(sector: true)
  scope :normal,  where(:sector.ne => true)

  scope :by_texts_size, order_by(:text_ids => :desc)
#  sort_by { |kw| kw.texts.size }

#  def self.by_texts_size
    #includes(:texts).sort_by { |kw| kw.texts.size }
  #end

  mapping do
    indexes :created_at, :type => "date",    :index => "no", :include_in_all => false
    indexes :updated_at, :type => "date",    :index => "no", :include_in_all => false
                         
    indexes :sector,     :type => "boolean"
                         
    indexes :name_en,    :type => "string",  :boost => 5
    indexes :name_cn,    :type => "string",  :boost => 5

    indexes :work_task,                  type: "string",  index: "not_analyzed"
    indexes :released,                   type: "boolean", index: "not_analyzed"
    
    indexes :date,                       type: "date",                           include_in_all: false
    indexes :overall_value_rating,       type: "integer",                        include_in_all: false
    indexes :client_ids,                 type: "string",  index: "not_analyzed"
    indexes :person_ids,                 type: "string",  index: "not_analyzed"
    indexes :source_ids,                 type: "string",  index: "not_analyzed"
    indexes :keyword_ids,                type: "string",  index: "not_analyzed"
    indexes :organisation_ids,           type: "string",  index: "not_analyzed"
    indexes :debate_ids,                 type: "string",  index: "not_analyzed"
  end
  
  def name
    "#{name_en} #{name_cn}"
  end

  def processed_name_en
    if name_en.downcase == "rmb" || name_en.downcase == "soe reform"
      parts = name_en.split(" ")
      val = ""
      parts.each_with_index do |p,ix|
        if ix == 0
          val += p.upcase
        else
          val += " "
          val += p.downcase
        end
      end
      return val
    elsif name_en.downcase == "laojiao" || name_en.downcase == "hukou"
      "<i>#{name_en.downcase}</i>".html_safe
    else
      name_en.downcase
    end
  end

  def connected_sector
    return self if sector
    debates.first.keywords.sectors.first
  end

  def object
    self
  end

  def date
    updated_at.to_date
  end
  def overall_value_rating
    # dynamically calculated, won't work from ES version because text_ids aren't indexed
    texts.map(&:overall_value_rating).compact.mean.try(:round)
  end
  def overall_value_rating_indexed
    # value as loaded from ES, rather than dynamically calculated
    self['overall_value_rating']
  end
  def keyword_ids
    [self.id]
  end
  def person_ids
    texts.map(&:person_ids).flatten.uniq
  end
  def source_ids
    texts.map(&:source_ids).flatten.uniq
  end
  def organisation_ids
    texts.map(&:organisation_ids).flatten.uniq
  end
  
  def sector?
    sector == true
  end

  def icon
    return case name_en.downcase
            when "social policy"
              "icon-A"
            when "resources"
              "icon-B"
            when "governance and law"
              "icon-C"
            when "external impact"
              "icon-D"
            when "climate"
              "icon-H"
            when "environment"
              "icon-E"
            when "energy"
              "icon-F"
            when "economy"
              "icon-G"
    end
  end

  def dss_class
    return case name_en.downcase
            when "social policy"
              "dss-social"
            when "resources"
              "dss-resources"
            when "governance and law"
              "dss-law"
            when "external impact"
              "dss-external"
            when "climate"
              "dss-climate"
            when "environment"
              "dss-environment"
            when "energy"
              "dss-energy"
            when "economy"
              "dss-econ"
    end
  end

# House - social policy 社会政策
# Truck - resources 资源
# Scales - governance and law 政法 
# World - external impact 走出去
# Sun - climate 气侯
# Tree - environment 环境
# Light bulb - energy 能源
# Chart - Economy 经济 

  def self.social
    sec = where(name_en: 'social policy').first
    if sec.nil?
      create!(name_en: 'social policy', name_cn: '社会政策', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '社会政策', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end

  def self.resources
    sec = where(name_en: 'resources').first
    if sec.nil?
      create!(name_en: 'resources', name_cn: '资源', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '资源', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end
  
  def self.law
    sec = where(name_en: 'governance and law').first
    if sec.nil?
      create!(name_en: 'governance and law', name_cn: '政法', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '政法', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end

  def self.external
    sec = where(name_en: 'external impact').first
    if sec.nil?
      create!(name_en: 'external impact', name_cn: '走出去', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '走出去', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end

  def self.climate
    sec = where(name_en: 'climate').first
    if sec.nil?
      create!(name_en: 'climate', name_cn: '气侯', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '气侯', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end

  def self.energy
    sec = where(name_en: 'energy').first
    if sec.nil?
      create!(name_en: 'energy', name_cn: '能源', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '能源', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end

  def self.economy
    sec = where(name_en: 'economy').first
    if sec.nil?
      create!(name_en: 'economy', name_cn: '经济', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '经济', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end

  def self.environment
    sec = where(name_en: 'environment').first
    if sec.nil?
      create!(name_en: 'environment', name_cn: '环境', sector: true, clients: [Client.first])
    else
      sec.update_attributes(name_cn: '环境', sector: true) if (sec.name_cn.nil? || sec.sector == false)
      sec
    end
  end

  private
  
  def reindex_debates
    (Array(debate_ids_was) + Array(debate_ids)).uniq.each do |debate_id|
      Debate.find(debate_id).update_index
    end
    ES.refresh # synchronization point
  end
  
  def reindex_types!
    reindex_clients
    reindex_texts
    reindex_debates
  end
end
