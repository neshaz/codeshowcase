class Person
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include TokenInput::HelpersForSelector
  extend  TokenInput::HelpersForSelected
  include ModelConcerns::Tire
  include ModelConcerns::SearchParameter
  include ModelConcerns::WorkItem
  include ModelConcerns::Reindex
  include ModelConcerns::Duplicate
  include ModelConcerns::UpdateConflict
  include ModelConcerns::Destroy

  CONGRESS  = ["9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th"]
  POLITBURO = CONGRESS.slice(6, 4)

  field :family_name_en,  :type => String
  field :given_name_en,   :type => String
  field :family_name_cn,  :type => String
  field :given_name_cn,   :type => String
  field :url,             :type => String
  field :personal_site_url, :type => String, :default => ""
  field :baidu_baike_url,   :type => String, :default => ""
  field :weibo_url,         :type => String, :default => ""
  field :qq,                :type => String, :default => ""
  field :tencent,           :type => String, :default => ""
  field :position_en,     :type => String
  field :position_cn,     :type => String
  field :background_and_reputation_en, :type => String
  field :background_and_reputation_cn, :type => String
  field :updated_by,      :type => String
  field :photo_uid,       :type => String
  field :picture_url,     :type => String

  field :joined_ccp,      :type => String
  field :birth_year,      :type => String
  field :death_year,      :type => String
  field :birth_place_en,  :type => String
  field :birth_place_cn,  :type => String
  
  field :education,       :type => String
  field :congress,        :type => String
  field :politburo_central, :type => String
  field :politburo_standing_committee, :type => String
  
  file_accessor :photo
  validates_property :mime_type, :of => :photo, :in => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png', 'image/gif'], :case_sensitive => false
  validate :handle_conflict, :only => :update
  
  validates_presence_of :clients
  #validates_uniqueness_of :family_name_en, :unless => :allow_duplicate
  #validates_uniqueness_of :family_name_cn, :unless => :allow_duplicate
  #validates_uniqueness_of :given_name_en, :unless => :allow_duplicate
  #validates_uniqueness_of :given_name_cn, :unless => :allow_duplicate

  embeds_many :career_points
  accepts_nested_attributes_for :career_points, :reject_if => :all_blank, :allow_destroy => true
  embeds_many :appointments
  accepts_nested_attributes_for :appointments, :reject_if => :all_blank, :allow_destroy => true
  embeds_many :appearances
  accepts_nested_attributes_for :appearances, :reject_if => :all_blank, :allow_destroy => true
  embeds_many :awards
  accepts_nested_attributes_for :awards, :reject_if => :all_blank, :allow_destroy => true
  embeds_many :education_points
  accepts_nested_attributes_for :education_points, :reject_if => :all_blank, :allow_destroy => true

  has_and_belongs_to_many :texts
  after_save :reindex_texts
  after_destroy :reindex_texts
  def texts=(new_texts)
    self.text_ids = new_texts.map(&:id)
  end

  has_and_belongs_to_many :clients
  after_save :reindex_clients
  def clients=(new_clients)
    self.client_ids = new_clients.map(&:id)
  end
  attr_checkbox_tokeninput :clients

  has_and_belongs_to_many :debates
  after_save :reindex_debates
  def debates=(new_debates)
    self.debate_ids = new_debates.map(&:id)
  end

  has_and_belongs_to_many :records
  after_save :reindex_records
  def records=(new_records)
    self.record_ids = new_records.map(&:id)
  end
  attr_tokeninput :records 

  mapping do
    indexes :created_at,                   :type => "date",    :index => "no", :include_in_all => false
    indexes :updated_at,                   :type => "date",    :index => "no", :include_in_all => false

    indexes :family_name_en,               :type => "string"
    indexes :given_name_en,                :type => "string"
    indexes :family_name_cn,               :type => "string"
    indexes :given_name_cn,                :type => "string"

    indexes :name,                         :type => "string",  :boost => 5
                                           
    indexes :url,                          :type => "string",  :index => "not_analyzed"
    indexes :personal_site_url,            :type => "string",  :index => "not_analyzed"
    indexes :baidu_baike_url,              :type => "string",  :index => "not_analyzed"
    indexes :weibo_url,                    :type => "string",  :index => "not_analyzed"
    indexes :qq,                           :type => "string",  :index => "not_analyzed"
    indexes :tencent,                      :type => "string",  :index => "not_analyzed"
    indexes :photo_uid,                    :type => "string",  :index => "not_analyzed"

    indexes :position_en,                  :type => "string"
    indexes :position_cn,                  :type => "string"

    indexes :background_and_reputation_en, :type => "string",  :boost => 0.5
    indexes :background_and_reputation_cn, :type => "string",  :boost => 0.5
    
    indexes :work_task,                  type: "string",  index: "not_analyzed"
    indexes :released,                   type: "boolean", index: "not_analyzed"
    
    indexes :date,                       type: "date",                           include_in_all: false
    indexes :overall_value_rating,       type: "integer",                        include_in_all: false
    indexes :client_ids,                 type: "string",  index: "not_analyzed"
    indexes :person_ids,                 type: "string",  index: "not_analyzed"
    indexes :mentioned_in_text_ids,      type: "string",  index: "not_analyzed"
    indexes :source_ids,                 type: "string",  index: "not_analyzed"
    indexes :keyword_ids,                type: "string",  index: "not_analyzed"
    indexes :organisation_ids,           type: "string",  index: "not_analyzed"
    indexes :debate_ids,                 type: "string",  index: "not_analyzed"
    indexes :record_ids,                 type: "string",  index: "not_analyzed"
  end
    
  def name
    "#{name_en} #{name_cn}"
  end
  def name_en
    "#{family_name_en.try(:upcase)} #{given_name_en}"
  end
  def name_cn
    "#{family_name_cn}#{given_name_cn}"
  end

  def name_en_with_span
    "<span class='surname'>#{family_name_en}</span> #{given_name_en}".html_safe
  end

  def shortened_name_en(length)
    if name_en.length > length-3
      return name_en.first(length-3)+"..."
    else
      return name_en
    end
  end

  def shortened_name_cn(length)
    en_l = shortened_name_en(length).length
    if (length - en_l) < 0
      return "..."
    else
      return name_cn.first((length - en_l)/2)
    end
  end

  def object
    self
  end

  def date
    updated_at.to_date
  end
  def date_en
    I18n::Backend::Simple.new.localize(:en, date, :short) 
  end
  def date_cn
    I18n::Backend::Simple.new.localize(:cn, date, :long) 
  end

  def overall_value_rating
    # dynamically calculated, won't work from ES version because text_ids aren't indexed
    texts.map(&:overall_value_rating).compact.mean.try(:round)
  end
  def overall_value_rating_indexed
    # value as loaded from ES, rather than dynamically calculated
    self['overall_value_rating']
  end
  def person_ids
    [self.id]
  end
  def mentioned_in_text_ids
    Text.all.reject do |text| 
      !text.mentioned_person_ids.include?(self.id)
    end.map(&:id)
  end
  def mentioned_in_texts
    Text.all.reject do |text| 
      !text.mentioned_person_ids.include?(self.id)
    end
  end
  def keyword_ids
    texts.map(&:keyword_ids).flatten.uniq
  end
  def source_ids
    texts.map(&:source_ids).flatten.uniq
  end
  def organisation_ids
    texts.map(&:organisation_ids).flatten.uniq
  end
  
  def released_param(param, released)
    released ? param.true : param.false
  end
  
  def top_keywords(released, how_many)
    SearchQuery.new.add(released_param(SearchParameter::Released, released)).p1.add(SearchParameter::Person.new(self.id)).p1.top_facets("keyword", how_many)
  end

  def empty_profile?
    background_and_reputation_en.blank? && background_and_reputation_cn.blank?
  end

  private
  
  def reindex_debates
    (Array(debate_ids_was) + Array(debate_ids)).uniq.each do |debate_id|
      Debate.find(debate_id).update_index
    end
    ES.refresh # synchronization point
  end
  
  def reindex_records
    (Array(record_ids_was) + Array(record_ids)).uniq.each do |record_id|
      Record.find(record_id).update_index
    end
    ES.refresh # synchronization point
  end
  
  def reindex_types!
    reindex_clients
    reindex_texts
    reindex_debates
    reindex_records
  end
end
