# encoding: utf-8

module SidebarTextNavigationHelper

  def text_query?
    @previous_query.current_type == SearchParameter::Type::ALL_TYPES.second
  end
 
  def texts_navigable?
    !current_text_position.nil? && previous_results.count > 1 && !root_page_params?(previous_query_params)
  end

  def build_text_links?
    text_query? && texts_navigable?
  end

  def current_text_position
    previous_results.index(@text)
  end

  def previous_text
    previous_results[current_text_position.pred]
  end

  def previous_text_path
    text_path(previous_text)
  end

  def previous_text_link
    build_text_links? ? build_previous_link(previous_text_path) : build_previous_link("#")
  end

  def has_next_text?
    !previous_results[current_text_position.succ].nil?
  end

  def next_text
    has_next_text? ? previous_results[current_text_position.succ] : previous_results.first
  end

  def next_text_path
    text_path(next_text)
  end

  def next_text_link
    build_text_links? ? build_next_link(next_text_path) : build_next_link("#")
  end
  
end
