class User < ActiveRecord::Base
  before_save { self.email = email.downcase }

  INDIVIDUAL = "individual"
  COMPANY = "company"
  NON_PROFIT = "non-profit"

  attr_accessor :login

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
          :recoverable, :trackable, :timeoutable, :validatable,
          :omniauthable, :omniauth_providers => [:linked_in], :authentication_keys => [:login]

  # associations
  has_one :user_profile, :class_name => 'User::Profile', inverse_of: :user, dependent: :destroy
  has_one :user_identity, :class_name => 'User::Identity', inverse_of: :user, dependent: :destroy # currently we're only using LinkedIn
  has_one :assessment_insight, -> { order id: :desc }, class_name: 'Assessment::Insight', inverse_of: :user, dependent: :destroy
  has_one :assessment_workstyle, -> { order id: :desc }, class_name: 'Assessment::Workstyle', inverse_of: :user, dependent: :destroy

  has_many :company_board_members, :class_name => 'Company::BoardMember', dependent: :destroy
  has_many :company_account_managers, :class_name => 'Company::AccountManager', dependent: :destroy

  has_and_belongs_to_many :articles, :class_name => "Library::Article"

  # validations
  validates :email,     uniqueness: { case_sensitive: false }
  validates :username,  uniqueness: { case_sensitive: false }
  validates_presence_of :username
  validates_presence_of :user_profile
  validates_presence_of :email, length: { maximum: 255 }
  validates_length_of :username, :within => 6..20, too_long: 'pick a shorter name', too_short: 'pick a longer name'
  #validates :password, format: { with: /\A(?=.*[a-zA-Z])(?=.*[0-9]).{8,}\z/ }

  delegate :first_name, :last_name ,:gender, :imported_profile?, :name, :membership_type, :can_upgrade?, to: :user_profile

  # nested attrs
  accepts_nested_attributes_for :user_profile, allow_destroy: true

  # devise customization to allow for username sign up / sign in
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_h).first
    end
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  def is_admin?
    admin
  end

  # override of devise email. User specifically by devise invitable
  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver
  end

  def account_manager?
    Company::AccountManager.all.collect(&:user).include?(self)
  end

  def board_member?
    Company::BoardMember.all.collect(&:user).include?(self)
  end

  def to_s
    "#{first_name} #{last_name}".titleize
  end

  def guest?
    false
  end

  def companies
    (company_board_members.collect(&:company) + company_account_managers.collect(&:company)).uniq
  end

  def visitor?
    sign_in_count == 1
  end

end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default("")
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime
#  updated_at             :datetime
#  admin                  :boolean          default(FALSE)
#  invitation_token       :string(255)
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string(255)
#  invitations_count      :integer          default(0)
#  receive_newsletter     :boolean          default(FALSE)
#  username               :string
#
