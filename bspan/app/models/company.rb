class Company < ActiveRecord::Base

  #associations
  has_many :organization_memberships, as: :organization

  has_many :company_board_members, :class_name => 'Company::BoardMember', dependent: :destroy
  has_many :users, through: :company_board_members, dependent: :destroy

  has_many :company_account_managers, :class_name => 'Company::AccountManager', dependent: :destroy
  has_many :users, through: :company_account_managers, dependent: :destroy

  has_one :company_profile, :class_name => 'Company::Profile', inverse_of: :company, dependent: :destroy
  has_many :searches, :class_name => 'Company::Search', inverse_of: :company, dependent: :destroy

  # TODO not sure if this models the right relationship...
  has_many :company_member_invitations, :class_name => 'Company::MemberInvitation', inverse_of: :company, dependent: :destroy

  # delegates
  delegate :symbol, :description, :website, :industry, :headquarters, :employees,
           :company_type, :can_upgrade?, :membership_type, :imported?, :domain, to: :company_profile

  #validations
  validates :name, uniqueness: { case_sensitive: false }
  validates_presence_of :name, :company_profile
  validates_associated :company_profile

  # Cache is also expired in the related observer model
  # IMPORTANT if you change the key name make sure it's updated in the associated observer!
  def board_members
    Rails.cache.fetch("sql-cache/company/#{id}/board_members", expires_in: 24.hours ) do
      company_board_members
    end
  end

  # Cache is also expired in the related observer model
  # IMPORTANT if you change the key name make sure it's updated in the associated observer!
  def account_managers
    Rails.cache.fetch("sql-cache/company/#{id}/account-managers", expires_in: 24.hours ) do
      company_account_managers
    end
  end

  def account_manager?(user)
    account_managers.collect(&:user).include?(user)
  end

  def board_member?(user)
    board_members.collect(&:user).include?(user)
  end

  def company_members
    board_members + account_managers
  end

  def member?(user)
    company_members.collect(&:user).include?(user)
  end

  def board_member_only?(user)
    board_member?(user) && !account_manager?(user)
  end

  def account_manager_only?(user)
    !board_member?(user) && account_manager?(user)
  end

  def board_members_only
    board_members.where.not( email: account_managers.pluck(:email)).collect(&:user)
  end

  def assessment_taken
    board_members.map {|u| u.assessment_board_insight }.count
  end
end

# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  name       :string(255)      not null
#  active     :boolean          default(FALSE)
#  welcomed   :boolean          default(FALSE), not null
#
