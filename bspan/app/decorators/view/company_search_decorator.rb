module View

  class CompanySearchDecorator < Decorator
    attr_reader :decorated_obj

    def initialize(search)
      @decorated_obj = search
    end

    def updated_at_date
      updated_at.strftime("%B #{updated_at.day}, %Y")
    end

    def responsibility_print
      case responsibility.try(:to_sym)
      when :fill_seat then 'Fill seat'
      when :fill_vacancy then 'Fill vacancy'
      when :other then responsibility_explain
      when :be_nominated then
        'Be nominated on ' << month << ". " << year
      end
    end

    def other_responsibility_explain_or_no_answer
      return other_responsibility_explain if other_responsibility_explain && !other_responsibility_explain.blank?
      'N/A'
    end

    def expertise_list_print
      list = print_list(expertise_list)
      list.gsub! '<li>Vision </li><li> goals</li>', '<li>Vision & goals</li>' if expertise_list.try(:include?, "Vision & goals")
      list.try(:html_safe)
    end

    def quality_list_print
      print_list(quality_list)
    end

    def company_interests
      print_list(interests)
    end

    def experience_description_print
      display_string = "<div class='font-medium columns small-12 margin-top-medium'>" << experience_description << "</div>" if experience_description
      display_string.try(:html_safe)
    end

    def website_info
      "More information about board members is available on the website (#{website})".html_safe if posted_director_bios
    end

    def organization_size
      organizational_size.downcase!
      organizational_size.eql?("no preference") ? "." : ", and experience in companies of #{organizational_size}."
    end

    def board_member_list
      members = board_members

      display_string = []
      if members.kind_of?(Array)
        members.each_with_index do |board_member,i|
          display_string << "<span data-bound=board_member_#{i}>" + board_member + "</span>"
        end
      end

      display_string = display_string.join(", ")
      display_string.html_safe

    end

    def functionl_experience_print
      display_string = "<ul class='margin-top-large'><li>"
      display_string << (!functional_experience || functional_experience.blank? ? 'N/A' : functional_experience)
      display_string << "</li></ul>"
      display_string.html_safe
    end
      
    def actively_seeking
      display_string = "Presently, #{company_name} has #{board_members.try(:count)} board members "
      display_string << "and is actively seeking: <br>#{new_director_seeking_sentence}" if seeking_board_members_type
      display_string.html_safe
    end

    def change_interest_text(interest)
      case interest
      when 'consumer'
           'consumer products and services'
      when 'industrials'
           'industrial products'
      when 'non-profit'
           'non-profits'
      when 'healthcare'
           'healthcare organizations'
      when 'professional services'
           'professional services organizations'
      else
          interest
      end
    end

    def gender_preference_print
      return 'None' if gender_preference.try(:to_sym) == :no_preference 
      gender_preference.try(:capitalize)
    end

    def company_type_string
      case company_type.to_sym
      when :private_company then 'Private'
      when :public_company then 'Public'
      when :non_profit_company then 'Non Profit'
      else
        'N/A'
      end
    end

    def other_quality_description_print
      return other_quality_description if other_quality_description && !other_quality_description.blank?
      'N/A'
    end

    private 

    def print_list(list)
      return unless list
      display_string = "<ul class='margin-top-large'>"
      list.split('&').each do |list_el|
        display_string << "<li>#{list_el}</li>"
      end
      display_string << "</ul>"
      display_string.html_safe
    end

    def new_director_seeking_sentence
      case seeking_board_members_type
        when 10
          "To fill an additional seat"
        when 20
          "Needs additional #{seeking_board_members_value} board member"
        when 30
          "A replacement for a retiring member"
        when 40
          "Nominees to stand for election at it's shareholder meeting in #{seeking_board_members_value}"
      end      
    end
  end
end