(function (namespace, $, undefined) {
    'use strict';

    var slider_is_working = false;
    var BOARD_ITEMS = ["documents", "assessments", "recruitment", "candidates", "consulting", "boardspan", "library"];

    namespace.ready = function () {

        // slider
        $('.slider_holder__btn').mouseover(function (e) {
            start(e)
        });

        initHeader();
        initBoardMap();

        $('.slider_holder__border').mouseenter(function () {
            stopAnims();
            displaySliderArrows();
        });

        $('.slider_holder__border').mouseleave(function () {
            stopAnims();
            hideSliderArrows();
        });

        $('.slider_holder__arrow').on('click', function (e) {

            if (slider_is_working) {
                return;
            }
            slider_is_working = true;

            var current_el = $('.slider_holder__content.is_active')[0].id;
            var slide_num = parseInt(current_el.substr(current_el.length - 1));
            var num_of_slides = $('.slider_holder__content').length;

            if (e.target.className.includes("left_side")) {
                slide_num = (slide_num == 1) ? num_of_slides : slide_num - 1;
            }
            else if (e.target.className.includes("right_side")) {
                slide_num = (slide_num == num_of_slides) ? 1 : slide_num + 1;
            }

            var next_slide_id = current_el.slice(0, -1) + slide_num;
            changeCurrentSlide(next_slide_id);
        });

        //header
        $(window).on('mouseup', function (e) {
            e.stopPropagation();
            e.preventDefault();
            checkMenuButton(e);
        });
    };

    function initHeader() {

        $(window).on('scroll', function (e) {
            returnOldMenuBtn();

            var show_navbar_edge = 400;
            var hide_navbar_edge = 300;

            var $sticky = $('.sticky');

            if ($(this).scrollTop() >= show_navbar_edge) {
                console.log('first if');
                $sticky.css('opacity', 1);
            }
            else if ($(this).scrollTop() > hide_navbar_edge) {
                console.log('second if');

                $sticky.css('opacity', ($(this).scrollTop() - hide_navbar_edge) / (show_navbar_edge - hide_navbar_edge));
            }
            else {
                console.log('third if');

                $sticky.css('opacity', 1);

                // only reset menu if hidden content is not visible
                var loginHiddenContent = $(".js-login-container");
                var enrollHiddenContent = $(".js-enroll-container");

                if (loginHiddenContent.is(":hidden") && enrollHiddenContent.is(":hidden")) {
                    $sticky.removeClass('-white');

                    if ($(this).scrollTop() > hide_navbar_edge / 4) {
                        console.log('fourth if');

                        $sticky.removeClass('-transparent').addClass('-white');
                        $sticky.css('opacity', '0');

                        $('.js-logo-image-dark').show();
                        $('.js-logo-image-white').hide();
                    }
                    else {
                        console.log('fifth if');

                        $sticky.addClass('-transparent');
                        $sticky.css('opacity', '1');

                        $('.js-logo-image-dark').hide();
                        $('.js-logo-image-white').show();
                    }
                }
            }
        })
    }

    function initBoardMap() {
        var width = $('[data-board-together]').width();
        var itemSize = width / 4;

        for (var i = 0; i < BOARD_ITEMS.length; i++) {
            setHoverItemVisibility(BOARD_ITEMS[i], false);
        }

        boardItemsHover();
    }

    function boardItemsHover() {
        var hovered = null;
        $(".rectangle").hover(function (enter_event) {
            hovered = $(this).children().first().data("board-item");
            setItemVisibility(hovered, false);
            setHoverItemVisibility(hovered, true);
        }, function (out_event) {
            hovered = $(this).children().first().data("board-item");
            setItemVisibility(hovered, true);
            setHoverItemVisibility(hovered, false);
        })
    }

    function setItemVisibility(item, visible) {
        var select = $('[data-board-item="' + item + '"]');
        if (visible) {
            select.show()
        } else {
            select.hide()
        }
    }

    function setHoverItemVisibility(item, visible) {
        var select = $('[data-board-item-hover="' + item + '"]');
        if (visible) {
            select.show()
        } else {
            select.hide()
        }
    }

    var start = function (e) {
        var target = e.target;

        if (slider_is_working || target.className.includes('is_active')) {
            return;
        }

        slider_is_working = true;
        changeCurrentSlide(target.id.slice(0, -3));
    };

    function changeCurrentSlide(next) {
        $('#' + next + 'btn').addClass('is_active');
        $('.slider_holder__content.is_active').fadeOut("slow", function () {
            $('.slider_holder__content.is_active').removeClass('is_active');
            nextSlide(next);
        });
    }

    function nextSlide(next) {

        $("#" + next).fadeIn("slow", function () {
            $("#" + next).addClass('is_active');
            slider_is_working = false;
        });
    }

    function displaySliderArrows() {
        $('.slider_holder__arrow.left_side').animate({
            opacity: 1.0,
            left: 0
        }, 500);

        $('.slider_holder__arrow.right_side').animate({
            opacity: 1.0,
            right: 0
        }, 500);
    }

    function hideSliderArrows() {
        $('.slider_holder__arrow.left_side').animate({
            opacity: 0,
            left: -100
        }, 500);

        $('.slider_holder__arrow.right_side').animate({
            opacity: 0,
            right: -100
        }, 500);
    }

    function stopAnims() {
        $('.slider_holder__arrow.right_side').stop();
        $('.slider_holder__arrow.left_side').stop();
    }


    function checkMenuButton(e) {

        var menu_button_box;
        if ($(e.target).hasClass('fa-close')) {
            returnOldMenuBtn();
            return;
        }
        else if ($(e.target).hasClass('menu-btn')) {
            menu_button_box = $(e.target);
        }
        else if ($(e.target).parent().hasClass('menu-btn')) {
            menu_button_box = $(e.target).parent();
        }
        else {
            returnOldMenuBtn();
            return;
        }

        var dropdown_list = $('.top-bar__dropdown');
        if (dropdown_list.css('visibility') === 'visible')
            return;

        var dropdown_el = $('.home-dropdown li:nth-child(1)');
        var button_new_width = dropdown_el.outerWidth() + 2;
        var button_new_height = dropdown_el.outerHeight();

        dropdown_list.css({
            'visibility': 'visible'
        });

        menu_button_box.append("<i class='fa fa-close right'></i>");

        menu_button_box.css({
            'background-color': '#3f3f3f',
            '-webkit-box-shadow': 'inset 0 0 0 1px #5a5a5a',
            '-moz-box-shadow': 'inset 0 0 0 1px #5a5a5a',
            'box-shadow': 'inset 0 0 0 1px #5a5a5a',
            'text-align': 'left',
            'width': button_new_width,
            'height': button_new_height
        });
    }

    function returnOldMenuBtn() {
        var first_button = $('[data-topbar]').find(".button").first();
        var dropdown_list = $('.top-bar__dropdown');

        $('.menu-btn').css({
            'background-color': '',
            '-webkit-box-shadow': first_button.css('-webkit-box-shadow'),
            '-moz-box-shadow': first_button.css('-moz-box-shadow'),
            'box-shadow': first_button.css('box-shadow'),
            'text-align': '',
            'width': '',
            'height': ''
        });

        $('.menu-btn .fa-close').remove();

        dropdown_list.css({
            'visibility': 'hidden'
        });
    }

}(BSPAN.VIEW.HOME = BSPAN.VIEW.HOME || {}, jQuery));

$(document).on('page:load', BSPAN.VIEW.HOME.ready);
$(document).ready(BSPAN.VIEW.HOME.ready);
