/*
 * This script is used for BOTH the individual assessment and the expertise part of the board assessment.
 */
(function (assessment, $, undefined) {
    'use strict';

    assessment.insight = function () {
        init();

        var elem = document.getElementById('insight-form');
        var event = new Event('submit');  // (*)

        $('.confirm-modal').click( function(e) {
            e.preventDefault();

            closeModal();

            elem.dispatchEvent(event);
            // not sure why but the form is being submitted twice.
            // removing the event listener after the first submit
            // prevents the form being submitted more than once.
            // N.B. the event listener is registered when the confirmation modal appears
        //    elem.removeEventListener('submit', submitHandler, false);
        });

        $('.close-modal-button').on('click', function(e) {
            closeModal();
        });

        $(document).ajaxError(errorHandler);

        $('.confirm-modal-reveal').on('click', function(e) {
            e.preventDefault();

            if(!invalidStrengthCount()) {
                $('#insightsModal').foundation('reveal', 'open');

                // add event listener when confirmation modal appears
                elem.addEventListener('submit', submitHandler, false);
            }
        });
    };

    /*
     *  Private variables and methods below here please
     */

    var MAX_INDIVIDUAL_STRENGTHS = 5;
    var $selector;

    var init = function () {
        $selector = new BSPAN.SelectorCache();
            $selector.get('[data-sortable-1]').sortable({
                connectWith: ".connectedSortable",
                stop: preventMaxStrengthSelection
            }).disableSelection();

            $selector.get('[data-sortable-2]').sortable({
                connectWith: ".connectedSortable",
                receive: removePlaceholder,
                remove: addPlaceholder,
                over: hidePlaceholder,
                out: showPlaceholder,
                cancel: ".list-placeholder"
            }).disableSelection();

        $selector.get('[data-comment]').empty();
    };

    var submitHandler = function (event) {
        $.ajax({
            url: this.action,
            type: "POST",
            dataType: "json",

            data: {
                insightStrengthSelection: individualStrengthSelectionStr(),
                insightComment: $($selector.get('[data-comment]')).val()
            },

            success: function (result) {
                console.log("Redirecting to: " + result.path);
                window.location = result.path;
            }
        });

        return false;
    };

    var individualStrengthSelectionStr = function () {
        var strengthSelection = [];

        $selector.get('[data-sortable-2]').find('li.ui-state-default').each(function () {
            strengthSelection.push($(this).data("strength"));
        });

        return strengthSelection.join('&');
    };

    var removePlaceholder = function (event, ui) {
        $( $selector.get('[data-sortable-2]').find('li.list-placeholder')[0] ).remove();

        var number_of_selected = $selector.get('[data-sortable-2]').find('li.draggable_strength').length;
        if (number_of_selected >= MAX_INDIVIDUAL_STRENGTHS) {
            removeStrengthsError();
        }
    };

    var addPlaceholder = function (e, ui) {
        var number_of_items = $selector.get('[data-sortable-2]').find('li').length;

        if (number_of_items < MAX_INDIVIDUAL_STRENGTHS) {
            $selector.get('[data-sortable-2]').append('<li class="list-placeholder">Drag from the list to your left</li>');
        }
    };

    var hidePlaceholder = function (event, ui) {
        $($selector.get('[data-sortable-2]').find('li.list-placeholder')[0]).hide();
    };

    var showPlaceholder = function (event, ui) {
        $($selector.get('[data-sortable-2]').find('li.list-placeholder')[0]).show();
    };

    // ensure the user is submitting the required number of individual strengths
    var invalidStrengthCount = function () {
        if ( $selector.get('[data-sortable-2]').find('li.ui-state-default').length >= MAX_INDIVIDUAL_STRENGTHS ) {
            return false;
        }

        $selector.get('[data-error-msg]').append(errorMsg("Please add your top " + MAX_INDIVIDUAL_STRENGTHS + " strengths before proceeding."));
        return true;
    };

    // prevents user from selecting more than five strengths
    var preventMaxStrengthSelection = function (ev, ui) {
        if ( $selector.get('[data-sortable-2]').find('li.ui-state-default').length > MAX_INDIVIDUAL_STRENGTHS ) {
            $selector.get('[data-sortable-1]').sortable('cancel');
        }
    };

    var errorHandler = function (event, request, settings) {
        $selector.get('[data-error-msg]').append(errorMsg($.parseJSON(request.responseText).errors));
    };

    var errorMsg = function (msg) {
        return "<li>" + msg + "</li>";
    };

    var removeStrengthsError = function () {
        $selector.get('[data-error-msg]').empty();
    };

    var closeModal = function() {
        $('#insightsModal').foundation('reveal', 'close');
    }

}(BSPAN.VIEW.ASSESSMENT = BSPAN.VIEW.ASSESSMENT || {}, jQuery));

$(document).on('page:load', BSPAN.VIEW.ASSESSMENT.insight);
$(document).ready(BSPAN.VIEW.ASSESSMENT.insight);