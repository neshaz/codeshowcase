/*
 * Responsible for all the mechanics of the 'importance' section of the board insight assessment.
 * TODO this class is ripe for refactoring as it's essentially a copy of workstyle.js
 */

(function (assessment, $, undefined) {
    'use strict';

    assessment.board_insight = function () {

        if ($('[data-board-insight-slider]').length > 0) {
            // the starting value  of each slider
            var SLIDER_RESTING_VAL = "1.50";

            // the number of pixels to scroll forward / backwards
            var ANIMATE = 905;

            var $selector = new BSPAN.SelectorCache();

            // hash of answers selected by user and submitted upon completion of the assessment
            var answers = {};

            var $question = $('div[data-board-insight-question]'),
                index = 0, //Starting index
                endIndex = $question.length - 1;

            console.log('end index: ' + endIndex);

            // Register event handlers
            $selector.get('body').on('click', 'button.next.active', nextBtnHandler);
            $('button[data-back]').click(backBtnHandler);
            toggleNextBtn($selector, SLIDER_RESTING_VAL);
            submitAnswers(answers);
        }

        // private stuff below here ...

        // next btn click handler
        function nextBtnHandler (event) {
            event.preventDefault();

            var questionId = $(this).data().boardInsightQuestion;

            // there are multiple sliders (one per question) and each has a
            // unique id taken from the question ORDER id
            var sliderVal = $selector.get('#board-insight-slider-' + questionId).val();

            if (sliderVal != SLIDER_RESTING_VAL) {

                if (index <= endIndex) {
                    recordCurrentAnswer();
                }

                if (index < endIndex) {
                    displayNextQuestion();
                    updateProgress();
                    incrementIndexes();
                    toggleActive.call(this);
                }
            }

            function recordCurrentAnswer() {
                answers['q' + questionId] = sliderVal;

                console.log('answers hash updated with question id: ' + questionId + ' and value: ' + sliderVal);
                console.log('answers hash: ' + JSON.stringify(answers));
            }

            function displayNextQuestion() {
                $question.animate({'left': "-=" + ANIMATE + "px"});
            }

            function updateProgress() {
                var percentComplete = ((questionId / ($question.length)) * 100).toFixed(0);
                var progressBarContainer = $("div[data-progress-bar='" + (questionId + 1) + "']");
                progressBarContainer.find('.progress-count > span').text(percentComplete + '%');
                progressBarContainer.find('.progress > span').css('width', percentComplete + '%');
            }

            function incrementIndexes() {
                questionId++;
                index++;
            }

            function toggleActive() {
                $(this).toggleClass('active');
                $selector.get("button.next[data-board-insight-question='" + questionId + "']").toggleClass('active');
            }
        }

        function backBtnHandler(event) {
            event.preventDefault();

            function toggleActive() {
                $('button.active').toggleClass('active');
                var questionId = $(this).data().boardInsightQuestion;
                console.log('Question ID before navigating back: ' + questionId);
                $selector.get("button.next[data-board-insight-question='" + --questionId + "']").toggleClass('active');
                console.log('Question ID after navigating back: ' + questionId);
            }

            if (index > 0) {
                index--;
                $question.animate({'left': "+=" + ANIMATE + "px"});

                toggleActive.call(this);
            }
        }
    };

    // ensure user cannot proceed unless they have moved slider from the resting position
    var toggleNextBtn = function ($selector, resting_val) {

        $('[data-board-insight-slider]').on({

            set: function () {
                var $this = $(this);
                var questionId = $this.data().sliderQuestion;
                var $nextBtn = $("button.active[data-board-insight-question='" + questionId + "']");

                $this.val() != resting_val ?
                    $nextBtn.prop("disabled", false) :
                    $nextBtn.prop("disabled", true);
            }

        });
    };

    var submitAnswers = function (answers) {

        $('button[data-board-insight-question-last]').click(function () {
            submit();
        });

        function submit() {
            console.log('Submitting the following answers: ' + JSON.stringify(answers));

            $.ajax({
                type: 'POST',
                url: '/assessments/board_insights/importance_answers',
                data: {'answers' : answers },

                success: function (result) {
                    window.location = result.path;
                },

                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                }
            });
        }
    };

}(BSPAN.VIEW.ASSESSMENT = BSPAN.VIEW.ASSESSMENT || {}, jQuery));

$(document).on('page:load', BSPAN.VIEW.ASSESSMENT.board_insight);
$(document).ready(BSPAN.VIEW.ASSESSMENT.board_insight);
