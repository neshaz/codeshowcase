class Assessments::InsightsController < AssessmentsController

  layout :resolve_assessment_layout

  def index
    load_assessments
  end

  def show
    @insight_decorator = View::InsightDecorator.new(load_assessment)
  end

  def details
    @insight_decorator = View::InsightDecorator.new(load_assessment)
  end

  def new
    @assessment = current_user.assessment_insight
  end

  def create
    build_assessment

    if save_assessment
      render json: { path: assessments_insight_path(@assessment), insight: @assessment }
    else
      render json: { errors: @assessment.errors.full_messages }, status: :unprocessable_entity
    end

  end

  private

  def load_assessments
    @assessments ||= assessment_scope.to_a
  end

  def load_assessment
    @assessment ||= assessment_scope.find(params[:id])
  end

  def build_assessment
    @assessment ||= assessment_scope.build
    @assessment.attributes = assessment_params
  end

  def save_assessment
    @assessment.save
  end

  def assessment_params
    strength_selection = params[:insightStrengthSelection].try(:split, "&")

    if strength_selection
      {
          assessment_insight_strengths: construct_strengths(strength_selection),
          user: current_user,
          comment: params[:insightComment],
          rank: strength_selection
      }
    end
  end

  def assessment_scope
    Assessment::Insight.all
  end

  def construct_strengths(arr)
    arr.inject([]) { |result, strength_id| result << Assessment::Insight::Strength.find(strength_id) }
  end

end